//Global
Texture2D ObjTexture;
SamplerState ObjSamplerState;
TextureCube SkyMap;

//Typedefs
struct Light
{

	float3 dir;
	float4 ambient;
	float4 diffuse;

};

cbuffer cbPerFrame
{
	Light light;
};

cbuffer cbPerObject
{
	float4x4 WVP;
	float4x4 World;
};

struct VS_OUTPUT
{
	float4 Pos : SV_POSITION;
	float4 worldPos : POSITION;
	float2 TexCoord : TEXCOORD;
	float3 normal : NORMAL;
};

struct SKYMAP_VS_OUTPUT    
{
	float4 Pos : SV_POSITION;
	float3 texCoord : TEXCOORD;
};

//--------------------------------------------------VS------------------------------------------------------------------
VS_OUTPUT VS(float4 inPos : POSITION, float2 inTexCoord : TEXCOORD, float3 normal : NORMAL)
{
	VS_OUTPUT output;

	output.Pos = mul(inPos, WVP);

	output.worldPos = mul(inPos, World);

	output.normal = mul(normal, World);

	output.TexCoord = inTexCoord;

	return output;
}

//--------------------------------------------------PS------------------------------------------------------------------
float4 PS(VS_OUTPUT input) : SV_TARGET
{

	input.normal = normalize(input.normal);

	float4 diffuse = ObjTexture.Sample(ObjSamplerState, input.TexCoord);
	clip(diffuse.a - 0.6);

	float3 finalColor;

	finalColor = diffuse * light.ambient;
	finalColor += saturate(dot(light.dir, input.normal) * light.diffuse * diffuse);

	return float4(finalColor, diffuse.a);
}

//--------------------------------------------------SKY VS------------------------------------------------------------------
SKYMAP_VS_OUTPUT SKYMAP_VS(float3 inPos : POSITION, float2 inTexCoord : TEXCOORD)
{
	SKYMAP_VS_OUTPUT output = (SKYMAP_VS_OUTPUT)0;

	output.Pos = mul(float4(inPos, 1.0f), WVP).xyww;

	output.texCoord = inPos;

	return output;
}

//--------------------------------------------------SKY PS------------------------------------------------------------------
float4 SKYMAP_PS(SKYMAP_VS_OUTPUT input) : SV_Target
{
	return SkyMap.Sample(ObjSamplerState, input.texCoord);
}

//--------------------------------------------------LIGHT PS------------------------------------------------------------------
float4 D2D_PS(VS_OUTPUT input) : SV_TARGET
{
	float4 diffuse = ObjTexture.Sample(ObjSamplerState, input.TexCoord);
	clip(diffuse.a - 0.6);
	return diffuse;
}