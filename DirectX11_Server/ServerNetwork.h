#pragma once
#include <winsock2.h>
#include <Windows.h>
#include "NetworkServices.h"
#include <ws2tcpip.h>
#include "NetworkData.h"
#include <map> 

using namespace std;

#pragma comment (lib, "Ws2_32.lib")

#define SAFE_RELEASE(p) { if ( (p) ) { (p)->Release(); (p) = 0; } }
#define SAFE_DELETE(a) if( (a) != NULL ) delete (a); (a) = NULL;

//define buffer size and port
#define DEFAULT_BUFLEN 512
#define DEFAULT_PORT "27010"

class ServerNetwork {
public:
	//Socket to listen new connection
	SOCKET ListenSocket;
	//Socket given to clients
	SOCKET ClientSocket;
	//error checking
	int iResult;
	//table for keeping trakc of each clients socket
	std::map<unsigned int, SOCKET> sessions;

public:
	ServerNetwork();
	bool acceptNewClient(unsigned int &id);

	//receive data
	int receiveData(unsigned int client_id, char* recvbuf);

	//broadcast to all clients
	void sendToAll(char* packets, int totalSize);

};