#include "ServerGame.h"

ServerGame::ServerGame() {
	
	client_id = 1;

	m_server = new ServerNetwork();

	//m_StepTimer.SetFixedTimeStep(true);
	//m_StepTimer.SetTargetElapsedSeconds(1.f / 60.f);
}

void ServerGame::RunServer() {

	//m_StepTimer.Tick([&](){
		//Update(m_StepTimer);
	//});

	Update();

}

void ServerGame::Update() {
//void ServerGame::Update(DX::StepTimer const& m_StepTimer) {


	// get new clients
	if (m_server->acceptNewClient(client_id))
	{
		printf("client %d has been connected to the server\n", client_id);

		client_id++;

	}

	receiveFromClients();

}

void ServerGame::receiveFromClients() {
	
	//through all clients
	for (iter = m_server->sessions.begin(); iter != m_server->sessions.end(); iter++) {

		//get data from client
		int data_length = m_server->receiveData(iter->first, network_data);

		if (data_length <= 0)
			continue; // no data recieved

		
		for(int i = 0; i < (unsigned int)data_length; ){

			packet.deserialize(&(network_data[i]));
			i += sizeof(Packet);

			switch (packet.packet_type) {
				case PacketTypes::INIT_CONNECTION:	
						Give_PlayerID();
					break;
				
				case PacketTypes::INIT_PLAYER:
						Init_PlayerData();	
					break;

				case PacketTypes::PLAYER_READY:
						Check_PlayerReady();
					break;

				case PacketTypes::ACTION_EVENT:
						ReceiveAndSend_Input();
					break;

				case PacketTypes::SHOOT:
						Normal_Shooting();
					break;

				case PacketTypes::PLAYER_HIT:
						Hit_Player();
					break;

				case PacketTypes::HEALING:
						Healing_Player();
					break;

				case PacketTypes::SHOOT_ULTIMATE:
						Ultimate_Shooting();
					break;

				case PacketTypes::GAME_END:
						Find_TheWinner();
					break;

				default:
					//MessageBox(NULL, L"SEVER SIDE: Error packet type", L"Error", MB_OK | MB_ICONERROR);
					printf("SEVER SIDE: Error packet type \n");
					break;
				}

		}
	}
}

void ServerGame::sendPackets(PacketTypes packet_type) {

	//Send action packet
	const unsigned int packet_size = sizeof(Packet);
	char packet_data[packet_size];

	packet.packet_type = packet_type;

	packet.serialize(packet_data);
	m_server->sendToAll(packet_data, packet_size);
}

void ServerGame::Give_PlayerID(){

	//Get initial connection from clients and give them an ID
	for (std::map<unsigned int, SOCKET>::iterator iter_Player = m_server->sessions.begin(); iter_Player != m_server->sessions.end(); iter_Player++) {
		const unsigned int packet_size = sizeof(Packet);
		char packet_data[packet_size];
		packet.packet_type = PacketTypes::INIT_CONNECTION;

		packet.player_Id = iter_Player->first;

		packet.serialize(packet_data);
		NetworkServices::sendMessage(iter_Player->second, packet_data, packet_size);

		printf("server received init packet from client NUMBER: %d \n", packet.player_Id);
		_RPTF1(0, "GET INIT PACKET from CLIENT number: %d \n", packet.player_Id);
	}

}

void ServerGame::Init_PlayerData() {

	//Give initial data to each player 
	const unsigned int packet_size = sizeof(Packet);
	char packet_data[packet_size];

	packet.packet_type		= PacketTypes::INIT_PLAYER;
	packet.player_Id		= iter->first;
	packet.NumOfPlayers		= client_id - 1;
	packet.NumAlivePlayers	= packet.NumOfPlayers;
	packet.AllPlayersAreReady = false;
	packet.ID_OF_WINNER		= 0;
	packet.ID_OF_HEALING	= 0;
	packet.ID_OF_SHOOTING	= 0;
	packet.ID_OF_ULTIMATE	= 0;

	for (int i = 0; i < packet.NumOfPlayers; i++) {
		packet.PlayerData[i].isAlive	= true;
		packet.PlayerData[i].HP			= 100;
		packet.PlayerData[i].Energy		= 100;
		packet.PlayerData[i].AmountOfShield = 100;
		packet.PlayerData[i].NumOfHealing	= 1;
		packet.PlayerData[i].UltimateGauge	= 0;
		packet.PlayerData[i].isHitWithUltimate = false;
		packet.PlayerData[i].UltiReady	= false;
	}

	packet.serialize(packet_data);
	NetworkServices::sendMessage(iter->second, packet_data, packet_size);

	_RPTF1(0, "INIT PLAYERS: %d \n", packet.player_Id);
	_RPTF1(0, "NUM OF ALIVE PLAYERS: %d \n", packet.NumAlivePlayers);
	printf("INIT PLAYERS: %d \n", packet.player_Id);

}

void ServerGame::Check_PlayerReady() {

	int Ready_P = 0;

	for (int NumPlayers : index_num_player) {

		if (packet.NumOfPlayers == NumPlayers) {
			//Check ready status of each player and then plus by 1 if that player is ready
			for (int i = 0; i < packet.NumOfPlayers; i++) {
				if (packet.PlayerData[i].ThisPlayerIsReady == true) {
					Ready_P += 1;
					continue;
				}
			}
			if (Ready_P == packet.NumOfPlayers)
				packet.AllPlayersAreReady = true;
			else
				packet.AllPlayersAreReady = false;
		}


	}

	//printf("PLAYERS %d READY!!!!\n", packet.player_Id);
	sendPackets(PacketTypes::PLAYER_READY);

}

void ServerGame::ReceiveAndSend_Input() {

	//Main packet that has been sent from clients and server boardcast to all clients in order to update same data 
	for (std::map<unsigned int, SOCKET>::iterator iter_Player = m_server->sessions.begin(); iter_Player != m_server->sessions.end(); iter_Player++) {

		packet.player_Id = iter_Player->first;

		if (packet.PlayerData[iter_Player->first - 1].Energy < 100) {
			packet.PlayerData[iter_Player->first - 1].Energy += 0.5f;
		}
		else
			packet.PlayerData[iter_Player->first - 1].Energy += 0.0f;

		printf("SEVER received ACTION from client number: %d\n", packet.player_Id);
	}
	sendPackets(PacketTypes::GET_INPUT);

}

void ServerGame::Normal_Shooting() {

	//Reduce shooting energy
	for (int i = 0; i < packet.NumOfPlayers; i++) {

		if (packet.ID_OF_SHOOTING == i + 1) {
			if (packet.PlayerData[i].Energy == 100)
				packet.PlayerData[i].Energy -= 100;
			else
				packet.PlayerData[i].Energy += 0.0f;
		}
	}
	_RPTF1(0, "client number: %d is Shooting\n", packet.ID_OF_SHOOTING);
	printf("client number: %d is Shooting\n", packet.ID_OF_SHOOTING);
	sendPackets(PacketTypes::SHOOT);

}

void ServerGame::Ultimate_Shooting() {

	//Reduce Ultimate energy
	for (int i = 0; i < packet.NumOfPlayers; i++) {

		if (packet.ID_OF_ULTIMATE == i + 1) {
			if (packet.PlayerData[i].UltimateGauge == 100) {
				packet.PlayerData[i].UltimateGauge -= 100;
				packet.PlayerData[i].UltiReady = true;
			}
			else
				packet.PlayerData[i].UltiReady = false;

		}
	}
	_RPTF1(0, "client number: %d is shooting ultimate skill\n", packet.ID_OF_ULTIMATE);
	printf("client number: %d is Healing\n", packet.ID_OF_HEALING);
	sendPackets(PacketTypes::SHOOT_ULTIMATE);

}

void ServerGame::Hit_Player() {

	//Reduce clients HP or armor when get hit
	for (int i = 0; i < packet.NumOfPlayers; i++) {

		if (packet.ID_OF_HIT_PLAYER == i + 1) {

			
			//Set limit of Ultimate gauge to 100
			if (packet.PlayerData[i].UltimateGauge >= 100) 
				packet.PlayerData[i].UltimateGauge = 100;
			else
				//if player get hit ultimate % will be increased by 20%
				packet.PlayerData[i].UltimateGauge += 20;

			//Decrease armor
			if (packet.PlayerData[i].isShieldActive && packet.PlayerData[i].AmountOfShield != 0) {
				if (packet.PlayerData[i].AmountOfShield != 0) {
					if (!packet.PlayerData[i].isHitWithUltimate)
						packet.PlayerData[i].AmountOfShield -= 10;
					else
						packet.PlayerData[i].AmountOfShield -= 50;
				}
				else
					packet.PlayerData[i].AmountOfShield = 0;
			}
			//Decrease player HP
			else {
				//Get hit by normal shoot 
				if (!packet.PlayerData[i].isHitWithUltimate)
					packet.PlayerData[i].HP -= 10;
				else
					//Hit by Ultimate
					packet.PlayerData[i].HP -= 25;
			}

		}
	}

	_RPTF1(0, "client number: %d is HIT\n", packet.ID_OF_HIT_PLAYER);
	printf("client number: %d is HIT\n", packet.ID_OF_HIT_PLAYER);
	packet.ID_OF_HIT_PLAYER = 0;
	sendPackets(PacketTypes::PLAYER_HIT);

}

void ServerGame::Healing_Player() {

	//Increase client's HP
	for (int i = 0; i < packet.NumOfPlayers; i++) {

		if (packet.ID_OF_HEALING == i + 1) {
			if (packet.PlayerData[i].NumOfHealing > 0) {
				packet.PlayerData[i].HP += 50;
				packet.PlayerData[i].NumOfHealing -= 1;

				if (packet.PlayerData[i].HP > 100)
					packet.PlayerData[i].HP = 100;
			}
			else {
				packet.PlayerData[i].NumOfHealing = 0;
				packet.PlayerData[i].Healing = false;
			}
		}
	}
	_RPTF1(0, "client number: %d is Healing\n", packet.ID_OF_HEALING);
	printf("client number: %d is Healing\n", packet.ID_OF_HEALING);
	sendPackets(PacketTypes::HEALING);

}

void ServerGame::Find_TheWinner() {

	//Find the last standing player (Winner)
	for (int i = 0; i < packet.NumOfPlayers; i++) {

		if (packet.PlayerData[i].isAlive == true)
			packet.ID_OF_WINNER = i + 1;

	}
	_RPTF1(0, "WINNER IS CLIENT NUMBER: %d \n", packet.ID_OF_WINNER);
	//printf("WINNER IS CLIENT NUMBER: %d \n", packet.ID_OF_WINNER);
	sendPackets(PacketTypes::GAME_END);

}

void ServerGame::CleanUp() {

	SAFE_DELETE(m_server);

}