#include "ServerNetwork.h"
ServerNetwork::ServerNetwork() {

	WSADATA wsaData;

	ListenSocket = INVALID_SOCKET;
	ClientSocket = INVALID_SOCKET;

	//address info for socket to connect to
	struct addrinfo *result = NULL;
	struct addrinfo	hints;

	//init Winsock
	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0) {
		MessageBox(NULL, L"SEVER SIDE: WSAStartup failed with error", L"Error", MB_OK | MB_ICONERROR);
		exit(1);
	}

	//Set address info
	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	hints.ai_flags = AI_PASSIVE;

	// Resolve the server address and port
	iResult = getaddrinfo(NULL, DEFAULT_PORT, &hints, &result);
	if (iResult != 0) {
		MessageBox(NULL, L"SEVER SIDE: getaddrinfo failed with error", L"Error", MB_OK | MB_ICONERROR);
		WSACleanup();
		exit(1);
	}

	// Create a SOCKET for connecting to server
	ListenSocket = socket(result->ai_family, result->ai_socktype, result->ai_protocol);
	if (ListenSocket == INVALID_SOCKET) {
		MessageBox(NULL, L"SEVER SIDE: socket failed with error", L"Error", MB_OK | MB_ICONERROR);
		freeaddrinfo(result);
		WSACleanup();
		exit(1);
	}

	// Set the mode of the socket to be nonblocking
	u_long iMode = 1;
	iResult = ioctlsocket(ListenSocket, FIONBIO, &iMode);

	if (iResult == SOCKET_ERROR) {
		MessageBox(NULL, L"SEVER SIDE: ioctlsocket failed with error", L"Error", MB_OK | MB_ICONERROR);
		closesocket(ListenSocket);
		WSACleanup();
		exit(1);
	}

	// Setup the TCP listening socket
	iResult = bind(ListenSocket, result->ai_addr, (int)result->ai_addrlen);
	if (iResult == SOCKET_ERROR) {
		MessageBox(NULL, L"SEVER SIDE: bind failed with error", L"Error", MB_OK | MB_ICONERROR);
		freeaddrinfo(result);
		closesocket(ListenSocket);
		WSACleanup();
		exit(1);
	}

	//free address info for server
	freeaddrinfo(result);

	// start listening for new clients attempting to connect
	iResult = listen(ListenSocket, SOMAXCONN);

	if (iResult == SOCKET_ERROR) {
		MessageBox(NULL, L"SEVER SIDE: listen failed with error", L"Error", MB_OK | MB_ICONERROR);
		closesocket(ListenSocket);
		WSACleanup();
		exit(1);
	}

}

bool ServerNetwork::acceptNewClient(unsigned int &id) {

	// if client waiting, accept the connection and save the socket
	ClientSocket = accept(ListenSocket, NULL, NULL);

	if (ClientSocket != INVALID_SOCKET)
	{
		//disable nagle on the client's socket
		char value = 1;
		setsockopt(ClientSocket, IPPROTO_TCP, TCP_NODELAY, &value, sizeof(value));

		// insert new client into session id table
		sessions.insert(pair<unsigned int, SOCKET>(id, ClientSocket));

		return true;
	}

	return false;
}


int ServerNetwork::receiveData(unsigned int client_id, char* recvbuf) {
	if (sessions.find(client_id) != sessions.end()) {
		SOCKET currentSocket = sessions[client_id];
		iResult = NetworkServices::receiveMessage(currentSocket, recvbuf, MAX_PACKET_SIZE);
		if (iResult == 0) {
			MessageBox(NULL, L"SEVER SIDE: Connection closed", L"Error", MB_OK | MB_ICONERROR);
			closesocket(currentSocket);
		}
		return iResult;
	}
	return 0;
}

void ServerNetwork::sendToAll(char* packets, int totalSize) {

	SOCKET currentSocket;
	std::map<unsigned int, SOCKET>::iterator iter;
	int iSendResult;

	for (iter = sessions.begin(); iter != sessions.end(); /*iter++*/)
	{
		currentSocket = iter->second;
		iSendResult = NetworkServices::sendMessage(currentSocket, packets, totalSize);

		if (iSendResult == SOCKET_ERROR)
		{
			//MessageBox(NULL, L"SEVER SIDE: Error SEVER send to ALL", L"Error", MB_OK | MB_ICONERROR);
			closesocket(currentSocket);
			sessions.erase(iter++);
		}
		else
			++iter;
	}
}

