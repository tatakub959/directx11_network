#pragma once
#include "ServerNetwork.h"
#include "NetworkData.h"
#include "StepTimer.h"
#include <vector>

// IDs for the clients connecting for table in ServerNetwork 
static unsigned int client_id;
static const std::vector<int> index_num_player = { 2 , 3 , 4 };

class ServerGame {

public:
	ServerGame();

	void RunServer();

	void Update();
	//void Update(DX::StepTimer const& m_StepTimer);

	void receiveFromClients();

	void sendPackets(PacketTypes packet_type);

	void Give_PlayerID();
	void Init_PlayerData();
	void Check_PlayerReady();
	void ReceiveAndSend_Input();
	void Normal_Shooting();
	void Ultimate_Shooting();
	void Hit_Player();
	void Healing_Player();
	void Find_TheWinner();


	void CleanUp();

private:
	Packet packet;
	HINSTANCE hInstance;
	//ServerNetwork object
	ServerNetwork* m_server;

	DX::StepTimer m_StepTimer;

	// data buffer
	char network_data[MAX_PACKET_SIZE];

	std::map<unsigned int, SOCKET>::iterator iter;

	unsigned int MaxPlayer = 4;
};