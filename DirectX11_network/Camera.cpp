#include "Camera.h"

CameraClass::CameraClass() {

	InitCamera();
}

void CameraClass::InitCamera() {

	//Camera information-------------------------------------------------------
	camTarget = XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f);
	camUp = XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f);

	//Set the View matrix
	camView = XMMatrixLookAtLH(camPosition, camTarget, camUp);

	//Set the Projection matrix
	camProjection = XMMatrixPerspectiveFovLH(0.4f*3.14f, (float)Width / Height, 1.0f, 1000.0f);
	
}

void CameraClass::MovingCamera(float moveLeftRight, float moveBackForward, float camYaw, float camPitch) {

	camRotationMatrix	= XMMatrixRotationRollPitchYaw(camPitch, camYaw, 0);
	camTarget			= XMVector3TransformCoord(DefaultForward, camRotationMatrix);
	camTarget			= XMVector3Normalize(camTarget);


	RotateYTempMatrix = XMMatrixRotationY(camYaw);

	//FPS-Camera
	camRight	 = XMVector3TransformCoord(DefaultRight, RotateYTempMatrix);
	camUp		 = XMVector3TransformCoord(camUp, RotateYTempMatrix);
	camForward	 = XMVector3TransformCoord(DefaultForward, RotateYTempMatrix);

	camPosition += moveLeftRight*camRight;
	camPosition += moveBackForward*camForward;

	camTarget = camPosition + camTarget;

	camView = XMMatrixLookAtLH(camPosition, camTarget, camUp);

}


