#pragma once
#include "d3d11.h"
#include <map> 
#include "Camera.h"
#include "Model.h"
#include "Blending.h"
#include "Light.h"
#include "LoadOBJ.h"
#include "Skybox.h"
#include "Player.h"
#include "UI.h"

using namespace std;

class GraphicsClass {

public:

	GraphicsClass::GraphicsClass(HWND hwnd);
	bool InitializeDirect3d11App(HWND hwnd);

	bool InitD2D_D3D101_DWrite(IDXGIAdapter1 *Adapter);
	void InitD2DScreenTexture();
	void RenderText(std::wstring text, ID3D11ShaderResourceView *d2dTexture, int inInt, Packet::Player_Data P_data);

	bool InitScene();
	void UpdateScene(int Timer, float delta_time, Packet packet, int P_ID, int UI_index);
	void DrawScene(PlayerClass* Player);
	void DrawSky();
	void DrawLoadedModel(LoadModelClass* m_LoadModel);
	void DrawUI(XMMATRIX cubeWorld, ID3D11ShaderResourceView* CubesTexture, ID3D11SamplerState* CubesTexSamplerState);
	void RenderGUI();
	void RenderStatusUI(int PlayerID);

	void CleanUp();


public:

	Packet PacketFromServer;

	unsigned int Player_ID;
	unsigned int fps;
	float delta_time;

	CameraClass* m_Camera;
	BlendingClass* m_Blending;
	LightClass* m_Light;
	SkyboxClass* m_Sky;
	LoadModelClass* m_LoadGround;
	UIclass* m_UI;


	//SetD3D11
	IDXGISwapChain* SwapChain;
	ID3D11Device* d3d11Device;
	ID3D11DeviceContext* d3d11DevCon;
	ID3D11RenderTargetView* renderTargetView;

	//Shader
	ID3D11VertexShader* VS;
	ID3D11PixelShader* PS;
	ID3D10Blob* VS_Buffer;
	ID3D10Blob* PS_Buffer;
	ID3D11InputLayout* vertLayout;

	//Texture
	ID3D11SamplerState* CubesTexSamplerState;
	//UI Texture
	ID3D11ShaderResourceView* titleTexture;
	ID3D11ShaderResourceView* playTexture;
	ID3D11ShaderResourceView* quitTexture;
	ID3D11ShaderResourceView* SelectionTexture;

	//Depth
	ID3D11DepthStencilView* depthStencilView;
	ID3D11Texture2D* depthStencilBuffer;

	//Text
	ID3D10Device1 *d3d101Device;
	IDXGIKeyedMutex *keyedMutex11;
	IDXGIKeyedMutex *keyedMutex10;
	ID2D1RenderTarget *D2DRenderTarget;
	ID2D1SolidColorBrush *Brush;
	ID3D11Texture2D *BackBuffer11;
	ID3D11Texture2D *sharedTex11;
	ID3D11Buffer *d2dVertBuffer;
	ID3D11Buffer *d2dIndexBuffer;
	IDWriteFactory *DWriteFactory;
	IDWriteTextFormat *TextFormat;
	IDWriteTextFormat *TextFormatHeader;

	//Texture for text in directX 2D
	ID3D11ShaderResourceView *d2dTexture;
	ID3D11ShaderResourceView *d2dTexture_crosshair;
	ID3D11ShaderResourceView *d2dTexture_blood;
	ID3D11ShaderResourceView *d2dTexture_Damage;
	ID3D11ShaderResourceView *d2dTexture_Waiting;
	ID3D11ShaderResourceView *d2dTexture_Winner;


	struct Vertex
	{
		Vertex() {}
		Vertex(float x, float y, float z, float u, float v, float nx, float ny, float nz, float tx, float ty, float tz) :
			pos(x, y, z), texCoord(u, v), normal(nx, ny, nz) {}

		XMFLOAT3 pos;
		XMFLOAT2 texCoord;
		XMFLOAT3 normal;

		XMFLOAT3 tangent;
		XMFLOAT3 biTangent;
	};

	//Create effects constant buffer's structure//
	ID3D11Buffer* cbPerObjectBuffer;
	XMMATRIX WVP;
	struct cbPerObject
	{
		XMMATRIX  WVP;
		XMMATRIX World;

		XMFLOAT4 difColor;
		BOOL hasTexture;
		BOOL hasNormMap;
	};
	cbPerObject cbPerObj;

	D3D11_INPUT_ELEMENT_DESC layout[4] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL",     0, DXGI_FORMAT_R32G32B32_FLOAT,    0, 20, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TANGENT", 0, DXGI_FORMAT_R32G32B32_FLOAT,    0, 32, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};
	UINT numElements = ARRAYSIZE(layout);

	UINT stride = sizeof(Vertex);
	UINT offset = 0;

};