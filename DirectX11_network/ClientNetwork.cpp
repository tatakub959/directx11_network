#include "ClientNetwork.h"

ClientNetwork::ClientNetwork() {
	
	WSADATA wsaData;

	//socket
	ConnectSocket = INVALID_SOCKET;

	//address info for socket to connect to
	struct addrinfo *result = NULL,
					*ptr = NULL,
					hints;

	//init Winsock
	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0) {
		MessageBox(NULL, L"CLIENT SIDE: WSAStartup failed with error", L"Error", MB_OK | MB_ICONERROR);
		exit(1);
	}

	//Set address info
	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;

	// Resolve the server address and port
	iResult = getaddrinfo(DEFAULT_IP, DEFAULT_PORT, &hints, &result);
	if (iResult != 0) {
		MessageBox(NULL, L"CLIENT SIDE: getaddrinfo failed with error", L"Error", MB_OK | MB_ICONERROR);
		WSACleanup();
		exit(1);
	}

	// Attempt to connect to an address until one succeeds
	for (ptr = result; ptr != NULL; ptr = ptr->ai_next) {

		// Create a SOCKET for connecting to server
		ConnectSocket = socket(ptr->ai_family, ptr->ai_socktype,
			ptr->ai_protocol);
		if (ConnectSocket == INVALID_SOCKET) {
			MessageBox(NULL, L"CLIENT SIDE: socket failed with error", L"Error", MB_OK | MB_ICONERROR);
			WSACleanup();
			exit(1);
		}

		// Connect to server.
		iResult = connect(ConnectSocket, ptr->ai_addr, (int)ptr->ai_addrlen);
		if (iResult == SOCKET_ERROR) {
			closesocket(ConnectSocket);
			ConnectSocket = INVALID_SOCKET;
			MessageBox(NULL, L"CLIENT SIDE: Can not connect to the server!", L"Error", MB_OK | MB_ICONERROR);
			continue;
		}
		break;
	}

	//free address info for server
	freeaddrinfo(result);

	// if connection failed
	if (ConnectSocket == INVALID_SOCKET) {
		MessageBox(NULL, L"CLIENT SIDE: Unable to connect to server!", L"Error", MB_OK | MB_ICONERROR);
		WSACleanup();
		exit(1);
	}

	//Set mode of socket to be nonblocking
	u_long iMode = 1;

	iResult = ioctlsocket(ConnectSocket, FIONBIO, &iMode);
	if (iResult == SOCKET_ERROR)
	{
		MessageBox(NULL, L"CLIENT SIDE: ioctlsocket failed with error", L"Error", MB_OK | MB_ICONERROR);
		closesocket(ConnectSocket);
		WSACleanup();
		exit(1);
	}

	//disable nagle
	char value = 1;
	setsockopt(ConnectSocket, IPPROTO_TCP, TCP_NODELAY, &value, sizeof(value));

};

int ClientNetwork::receivePackets(char* recvbuf) {

	iResult = NetworkServices::receiveMessage(ConnectSocket, recvbuf, MAX_PACKET_SIZE);
	if (iResult == 0) {
		MessageBox(NULL, L"CLIENT SIDE: Connection closed", L"Error", MB_OK | MB_ICONERROR);
		closesocket(ConnectSocket);
		WSACleanup();
	}
	return iResult;
}

