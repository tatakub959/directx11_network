#include "Graphics.h"

GraphicsClass::GraphicsClass(HWND hwnd) {

	m_Camera		= new CameraClass();
	m_Light			= new LightClass;
	m_LoadGround	= new LoadModelClass();
	m_UI			= new UIclass();

	InitializeDirect3d11App(hwnd);
	InitScene();
}

//-----------------------------------------------------Initialize Direct3d11App------------------------------------------------------------------------------------------------------
bool GraphicsClass::InitializeDirect3d11App(HWND hwnd) {

	//Describe our SwapChain Buffer
	DXGI_MODE_DESC bufferDesc;

	ZeroMemory(&bufferDesc, sizeof(DXGI_MODE_DESC));

	bufferDesc.Width	= Width;
	bufferDesc.Height	= Height;
	bufferDesc.RefreshRate.Numerator	= 60;
	bufferDesc.RefreshRate.Denominator	= 1;
	bufferDesc.Format	= DXGI_FORMAT_B8G8R8A8_UNORM;
	bufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	bufferDesc.Scaling	= DXGI_MODE_SCALING_UNSPECIFIED;

	//Describe our SwapChain
	DXGI_SWAP_CHAIN_DESC swapChainDesc;

	ZeroMemory(&swapChainDesc, sizeof(DXGI_SWAP_CHAIN_DESC));

	swapChainDesc.BufferDesc		= bufferDesc;
	swapChainDesc.SampleDesc.Count	= 1;
	swapChainDesc.SampleDesc.Quality = 0;
	swapChainDesc.BufferUsage		= DXGI_USAGE_RENDER_TARGET_OUTPUT;
	swapChainDesc.BufferCount		= 1;
	swapChainDesc.OutputWindow		= hwnd;
	swapChainDesc.Windowed			= TRUE;
	swapChainDesc.SwapEffect		= DXGI_SWAP_EFFECT_DISCARD;

	// Create DXGI factory to enumerate adapters
	IDXGIFactory1 *DXGIFactory;

	CreateDXGIFactory1(__uuidof(IDXGIFactory1), (void**)&DXGIFactory);

	// Use the first adapter    
	IDXGIAdapter1 *Adapter;

	DXGIFactory->EnumAdapters1(0, &Adapter);

	DXGIFactory->Release();

	//Create our Direct3D 11 Device and SwapChain
	D3D11CreateDeviceAndSwapChain(Adapter, D3D_DRIVER_TYPE_UNKNOWN, NULL, D3D11_CREATE_DEVICE_DEBUG | D3D11_CREATE_DEVICE_BGRA_SUPPORT,
		NULL, NULL, D3D11_SDK_VERSION, &swapChainDesc, &SwapChain, &d3d11Device, NULL, &d3d11DevCon);

	//Initialize Direct2D, Direct3D 10.1, DirectWrite
	InitD2D_D3D101_DWrite(Adapter);

	//Release the Adapter interface
	Adapter->Release();

	//Create our BackBuffer and Render Target
	SwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (void**)&BackBuffer11);
	d3d11Device->CreateRenderTargetView(BackBuffer11, NULL, &renderTargetView);

	//Describe our Depth/Stencil Buffer
	D3D11_TEXTURE2D_DESC depthStencilDesc;

	depthStencilDesc.Width		= Width;
	depthStencilDesc.Height		= Height;
	depthStencilDesc.MipLevels	= 1;
	depthStencilDesc.ArraySize	= 1;
	depthStencilDesc.Format		= DXGI_FORMAT_D24_UNORM_S8_UINT;
	depthStencilDesc.SampleDesc.Count	= 1;
	depthStencilDesc.SampleDesc.Quality = 0;
	depthStencilDesc.Usage		= D3D11_USAGE_DEFAULT;
	depthStencilDesc.BindFlags	= D3D11_BIND_DEPTH_STENCIL;
	depthStencilDesc.CPUAccessFlags = 0;
	depthStencilDesc.MiscFlags	= 0;

	//Create the Depth/Stencil View
	d3d11Device->CreateTexture2D(&depthStencilDesc, NULL, &depthStencilBuffer);
	d3d11Device->CreateDepthStencilView(depthStencilBuffer, NULL, &depthStencilView);

	return true;
}

//---------------------------------------------------------------InitD2D_D3D101_Dwrite---------------------------------------------------------------------------------
bool GraphicsClass::InitD2D_D3D101_DWrite(IDXGIAdapter1 *Adapter) {

	//Create our Direc3D 10.1 Device
	D3D10CreateDevice1(Adapter, D3D10_DRIVER_TYPE_HARDWARE, NULL, D3D10_CREATE_DEVICE_DEBUG | D3D10_CREATE_DEVICE_BGRA_SUPPORT,
		D3D10_FEATURE_LEVEL_9_3, D3D10_1_SDK_VERSION, &d3d101Device);

	//Create Shared Texture that Direct3D 10.1 will render on
	D3D11_TEXTURE2D_DESC sharedTexDesc;

	ZeroMemory(&sharedTexDesc, sizeof(sharedTexDesc));

	sharedTexDesc.Width			= Width;
	sharedTexDesc.Height		= Height;
	sharedTexDesc.Format		= DXGI_FORMAT_B8G8R8A8_UNORM;
	sharedTexDesc.MipLevels		= 1;
	sharedTexDesc.ArraySize		= 1;
	sharedTexDesc.SampleDesc.Count = 1;
	sharedTexDesc.Usage			= D3D11_USAGE_DEFAULT;
	sharedTexDesc.BindFlags		= D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_RENDER_TARGET;
	sharedTexDesc.MiscFlags		= D3D11_RESOURCE_MISC_SHARED_KEYEDMUTEX;

	d3d11Device->CreateTexture2D(&sharedTexDesc, NULL, &sharedTex11);

	// Get the keyed mutex for the shared texture (for D3D11
	sharedTex11->QueryInterface(__uuidof(IDXGIKeyedMutex), (void**)&keyedMutex11);

	// Get the shared handle needed to open the shared texture in D3D10.1
	IDXGIResource *sharedResource10;
	HANDLE sharedHandle10;

	sharedTex11->QueryInterface(__uuidof(IDXGIResource), (void**)&sharedResource10);

	sharedResource10->GetSharedHandle(&sharedHandle10);

	sharedResource10->Release();

	// Open the surface for the shared texture in D3D10.1
	IDXGISurface1 *sharedSurface10;

	d3d101Device->OpenSharedResource(sharedHandle10, __uuidof(IDXGISurface1), (void**)(&sharedSurface10));

	sharedSurface10->QueryInterface(__uuidof(IDXGIKeyedMutex), (void**)&keyedMutex10);

	// Create D2D factory
	ID2D1Factory *D2DFactory;
	D2D1CreateFactory(D2D1_FACTORY_TYPE_SINGLE_THREADED, __uuidof(ID2D1Factory), (void**)&D2DFactory);

	D2D1_RENDER_TARGET_PROPERTIES renderTargetProperties;

	ZeroMemory(&renderTargetProperties, sizeof(renderTargetProperties));

	renderTargetProperties.type = D2D1_RENDER_TARGET_TYPE_HARDWARE;
	renderTargetProperties.pixelFormat = D2D1::PixelFormat(DXGI_FORMAT_UNKNOWN, D2D1_ALPHA_MODE_PREMULTIPLIED);

	D2DFactory->CreateDxgiSurfaceRenderTarget(sharedSurface10, &renderTargetProperties, &D2DRenderTarget);

	sharedSurface10->Release();
	D2DFactory->Release();

	// Create a solid color brush to draw something with        
	D2DRenderTarget->CreateSolidColorBrush(D2D1::ColorF(1.0f, 1.0f, 0.0f, 1.0f), &Brush);

	//DirectWrite
	DWriteCreateFactory(DWRITE_FACTORY_TYPE_SHARED, __uuidof(IDWriteFactory),
		reinterpret_cast<IUnknown**>(&DWriteFactory));

	//Create normal text format
	DWriteFactory->CreateTextFormat(
		L"Arial",
		NULL,
		DWRITE_FONT_WEIGHT_REGULAR,
		DWRITE_FONT_STYLE_NORMAL,
		DWRITE_FONT_STRETCH_NORMAL,
		20.0f,
		L"en-us",
		&TextFormat
	);

	TextFormat->SetTextAlignment(DWRITE_TEXT_ALIGNMENT_LEADING);
	TextFormat->SetParagraphAlignment(DWRITE_PARAGRAPH_ALIGNMENT_NEAR);

	//Create normal text format
	DWriteFactory->CreateTextFormat(
		L"Script",
		NULL,
		DWRITE_FONT_WEIGHT_DEMI_BOLD,
		DWRITE_FONT_STYLE_ITALIC,
		DWRITE_FONT_STRETCH_NORMAL,
		25.0f,
		L"en-us",
		&TextFormatHeader
	);

	TextFormatHeader->SetTextAlignment(DWRITE_TEXT_ALIGNMENT_LEADING);
	TextFormatHeader->SetParagraphAlignment(DWRITE_PARAGRAPH_ALIGNMENT_NEAR);

	d3d101Device->IASetPrimitiveTopology(D3D10_PRIMITIVE_TOPOLOGY_POINTLIST);
	return true;
}

//---------------------------------------------------------InitD2DScreenTexture-------------------------------------------------------------------------------------------------------
void GraphicsClass::InitD2DScreenTexture()
{
	//Create the vertex buffer
	Vertex v[] =
	{
		// Front Face
		Vertex(-1.0f, -1.0f, -1.0f, 0.0f, 1.0f,-1.0f, -1.0f, -1.0f, 0.0f, 0.0f, 0.0f),
		Vertex(-1.0f,  1.0f, -1.0f, 0.0f, 0.0f,-1.0f,  1.0f, -1.0f, 0.0f, 0.0f, 0.0f),
		Vertex(1.0f,  1.0f, -1.0f, 1.0f, 0.0f, 1.0f,  1.0f, -1.0f, 0.0f, 0.0f, 0.0f),
		Vertex(1.0f, -1.0f, -1.0f, 1.0f, 1.0f, 1.0f, -1.0f, -1.0f, 0.0f, 0.0f, 0.0f),
	};

	DWORD indices[] = {
		// Front Face
		0,  1,  2,
		0,  2,  3,
	};

	D3D11_BUFFER_DESC indexBufferDesc;
	ZeroMemory(&indexBufferDesc, sizeof(indexBufferDesc));

	indexBufferDesc.Usage		= D3D11_USAGE_DEFAULT;
	indexBufferDesc.ByteWidth	= sizeof(DWORD) * 2 * 3;
	indexBufferDesc.BindFlags	= D3D11_BIND_INDEX_BUFFER;
	indexBufferDesc.CPUAccessFlags = 0;
	indexBufferDesc.MiscFlags	= 0;

	D3D11_SUBRESOURCE_DATA iinitData;

	iinitData.pSysMem = indices;
	d3d11Device->CreateBuffer(&indexBufferDesc, &iinitData, &d2dIndexBuffer);


	D3D11_BUFFER_DESC vertexBufferDesc;
	ZeroMemory(&vertexBufferDesc, sizeof(vertexBufferDesc));

	vertexBufferDesc.Usage		= D3D11_USAGE_DEFAULT;
	vertexBufferDesc.ByteWidth	= sizeof(Vertex) * 4;
	vertexBufferDesc.BindFlags	= D3D11_BIND_VERTEX_BUFFER;
	vertexBufferDesc.CPUAccessFlags = 0;
	vertexBufferDesc.MiscFlags	= 0;

	D3D11_SUBRESOURCE_DATA vertexBufferData;

	ZeroMemory(&vertexBufferData, sizeof(vertexBufferData));
	vertexBufferData.pSysMem = v;
	d3d11Device->CreateBuffer(&vertexBufferDesc, &vertexBufferData, &d2dVertBuffer);

	//Create A shader resource view from the texture D2D will render to,
	//So we can use it to texture a square which overlays our scene
	d3d11Device->CreateShaderResourceView(sharedTex11, NULL, &d2dTexture);
	D3DX11CreateShaderResourceViewFromFile(d3d11Device, L"..\\Assets\\Textures\\Crosshair_W.png", NULL, NULL, &d2dTexture_crosshair, NULL);
	D3DX11CreateShaderResourceViewFromFile(d3d11Device, L"..\\Assets\\Textures\\blood_PNG6093.png", NULL, NULL, &d2dTexture_blood, NULL);
	D3DX11CreateShaderResourceViewFromFile(d3d11Device, L"..\\Assets\\Textures\\damage.png", NULL, NULL, &d2dTexture_Damage, NULL);
	D3DX11CreateShaderResourceViewFromFile(d3d11Device, L"..\\Assets\\Textures\\wait.jpg", NULL, NULL, &d2dTexture_Waiting, NULL);
	D3DX11CreateShaderResourceViewFromFile(d3d11Device, L"..\\Assets\\Textures\\Winner.png", NULL, NULL, &d2dTexture_Winner, NULL);
}

//---------------------------------------------------------InitScene------------------------------------------------------------------------------------------------------------------
bool GraphicsClass::InitScene()
{
	//Load Ground
	if (!m_LoadGround->LoadObjModel(AssetPath + L"ground.obj", SwapChain, d3d11Device))
		return false;

	//init skybox
	m_Sky = new SkyboxClass(d3d11Device);
	m_Sky->CreateSphere(10, 10);

	//Init2D
	InitD2DScreenTexture();

	//Compile Shaders from shader file
	D3DX11CompileFromFile(L"..\\Assets\\Effects.hlsl", 0, 0, "VS", "vs_4_0", 0, 0, 0, &VS_Buffer, 0, 0);
	D3DX11CompileFromFile(L"..\\Assets\\Effects.hlsl", 0, 0, "PS", "ps_4_0", 0, 0, 0, &PS_Buffer, 0, 0);
	//Compile and crate light shader***********************************************
	m_Light->initLight(d3d11Device);

	//Create the Shader Objects
	d3d11Device->CreateVertexShader(VS_Buffer->GetBufferPointer(), VS_Buffer->GetBufferSize(), NULL, &VS);
	d3d11Device->CreatePixelShader(PS_Buffer->GetBufferPointer(), PS_Buffer->GetBufferSize(), NULL, &PS);


	//Set Vertex and Pixel Shaders
	d3d11DevCon->VSSetShader(VS, 0, 0);
	d3d11DevCon->PSSetShader(PS, 0, 0);

	//Create the Input Layout
	d3d11Device->CreateInputLayout(layout, numElements, VS_Buffer->GetBufferPointer(),
		VS_Buffer->GetBufferSize(), &vertLayout);

	//Set the Input Layout
	d3d11DevCon->IASetInputLayout(vertLayout);
	//Set Primitive Topology
	d3d11DevCon->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	D3DX11CreateShaderResourceViewFromFile(d3d11Device, L"..\\Assets\\Textures\\Wolf.png", NULL, NULL, &titleTexture, NULL);
	D3DX11CreateShaderResourceViewFromFile(d3d11Device, L"..\\Assets\\Textures\\start.png", NULL, NULL, &playTexture, NULL);
	D3DX11CreateShaderResourceViewFromFile(d3d11Device, L"..\\Assets\\Textures\\quit_butt.png", NULL, NULL, &quitTexture, NULL);
	D3DX11CreateShaderResourceViewFromFile(d3d11Device, L"..\\Assets\\Textures\\light-blue-plain-paper.jpg", NULL, NULL, &SelectionTexture, NULL);

	// Describe the Sample State
	D3D11_SAMPLER_DESC sampDesc;
	ZeroMemory(&sampDesc, sizeof(sampDesc));
	sampDesc.Filter		= D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	sampDesc.AddressU	= D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.AddressV	= D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.AddressW	= D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
	sampDesc.MinLOD = 0;
	sampDesc.MaxLOD = D3D11_FLOAT32_MAX;

	//Create the Sample State
	d3d11Device->CreateSamplerState(&sampDesc, &CubesTexSamplerState);

	//Create the Viewport
	D3D11_VIEWPORT viewport;
	ZeroMemory(&viewport, sizeof(D3D11_VIEWPORT));

	viewport.TopLeftX = 0;
	viewport.TopLeftY = 0;
	viewport.Width = Width;
	viewport.Height = Height;
	viewport.MinDepth = 0.0f;
	viewport.MaxDepth = 1.0f;

	//Set the Viewport
	d3d11DevCon->RSSetViewports(1, &viewport);

	//Create the buffer to send to the cbuffer in effect file
	D3D11_BUFFER_DESC cbbd;
	ZeroMemory(&cbbd, sizeof(D3D11_BUFFER_DESC));

	cbbd.Usage = D3D11_USAGE_DEFAULT;
	cbbd.ByteWidth = sizeof(cbPerObject);
	cbbd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	cbbd.CPUAccessFlags = 0;
	cbbd.MiscFlags = 0;

	d3d11Device->CreateBuffer(&cbbd, NULL, &cbPerObjectBuffer);

	//Blending setting
	m_Blending = new BlendingClass(d3d11Device);

	//Add shader for skybox
	m_Sky->initSky(m_Blending->cmdesc);

	//init UI
	m_UI->initUI(d3d11Device, d3d11DevCon, VS_Buffer);

	m_LoadGround->initGround();
	return true;

}

//---------------------------------------------------------RenderText------------------------------------------------------------------------------------------------------------------
void GraphicsClass::RenderText(std::wstring text, ID3D11ShaderResourceView *d2dTexture, int inInt, Packet::Player_Data P_data)
{
	std::wstring printAllPlayersInfo;
	std::wostringstream printString;

	std::wstring printPlayerInfo;
	std::wostringstream printStringPlayerInfo;

	std::wstring printPlayerAbilities;
	std::wostringstream printStringPlayerAb;

	//Release the D3D 11 Device
	keyedMutex11->ReleaseSync(0);

	//Use D3D10.1 device
	keyedMutex10->AcquireSync(0, 5);

	//Draw D2D content        
	D2DRenderTarget->BeginDraw();

	//Clear D2D Background
	D2DRenderTarget->Clear(D2D1::ColorF(0.0f, 0.0f, 0.0f, 0.0f));

	POINT mousePos;
	GetCursorPos(&mousePos);

	//-------------------------------------------ALL PLAYERS INFORMATION-------------------------
	printString << text << inInt << "\n"
		<< "Number of players: " << PacketFromServer.NumOfPlayers << "\n"
		<< "Alive Players: " << PacketFromServer.NumAlivePlayers << "\n"
		<< "P1 HP: " << PacketFromServer.PlayerData[0].HP << "\n"
		<< "P2 HP: " << PacketFromServer.PlayerData[1].HP << "\n"
		<< "P3 HP: " << PacketFromServer.PlayerData[2].HP << "\n"
		<< "P4 HP: " << PacketFromServer.PlayerData[3].HP << "\n";
	printAllPlayersInfo = printString.str();

	//-------------------------------------------PLAYERS INFORMATION-------------------------
	printStringPlayerInfo << "Player : " << Player_ID << "\n"
		<< "HP : " << (int)P_data.HP << "/100" << "\n";
	printPlayerInfo = printStringPlayerInfo.str();

	//-------------------------------------------PLAYERS ABILITIES-------------------------
	printStringPlayerAb
		<< "Energy : " << (int)P_data.Energy << "/100" << "\n"
		<< "Armor : " << P_data.AmountOfShield << "%" << "\n"
		<< "Healing : " << P_data.NumOfHealing << " Time" << "\n"
		<< "ULTIMATE SKILL : " << P_data.UltimateGauge << "%" << "\n";
	printPlayerAbilities = printStringPlayerAb.str();

	//Set the Font Color
	D2D1_COLOR_F White = D2D1::ColorF(1.0f, 1.0f, 1.0f, 1.0f);
	D2D1_COLOR_F Red = D2D1::ColorF(1.0f, 0.0f, 0.0f, 1.0f);
	D2D1_COLOR_F Green = D2D1::ColorF(0.8f, 1.0f, 0.5f, 1.0f);
	D2D1_COLOR_F Cyan = D2D1::ColorF(0.0f, 1.0f, 0.8f, 1.0f);


	//Create the D2D Render Area
	//Top-Left
	D2D1_RECT_F layoutRect = D2D1::RectF(0, 0, Width, Height);
	//Top-Right
	D2D1_RECT_F layoutRect_TR = D2D1::RectF((Width / 2) + 150, (Height / 2) - 250, Width, Height);
	//Buttom_Right
	D2D1_RECT_F layoutRect_BR = D2D1::RectF((Width / 2) + 100, (Height / 2) + 100, Width, Height);
	//Buttom_Left
	D2D1_RECT_F layoutRect_BF = D2D1::RectF(50, 200, Width, Height);

	//Set the brush color D2D will use to draw with
	Brush->SetColor(White);
	//Draw the Text for all player information
	D2DRenderTarget->DrawText(
		printAllPlayersInfo.c_str(),
		wcslen(printAllPlayersInfo.c_str()),
		TextFormat,
		layoutRect,
		Brush
	);

	Brush->SetColor(Green);
	//Draw the Text for player info
	D2DRenderTarget->DrawText(
		printPlayerInfo.c_str(),
		wcslen(printPlayerInfo.c_str()),
		TextFormatHeader,
		layoutRect_TR,
		Brush
	);

	Brush->SetColor(Cyan);
	//Draw the Text for abilities
	D2DRenderTarget->DrawText(
		printPlayerAbilities.c_str(),
		wcslen(printPlayerAbilities.c_str()),
		TextFormatHeader,
		layoutRect_BR,
		Brush
	);

	D2DRenderTarget->EndDraw();


	//Release the D3D10.1 Device
	keyedMutex10->ReleaseSync(1);

	//Use the D3D11 Device
	keyedMutex11->AcquireSync(1, 5);

	//Set the blend state for D2D render target texture objects
	d3d11DevCon->OMSetBlendState(m_Blending->Transparency, NULL, 0xffffffff);

	//Set d2d's pixel shader so lighting calculations are not done
	d3d11DevCon->PSSetShader(m_Light->D2D_PS, 0, 0);

	//Set the d2d Index buffer
	d3d11DevCon->IASetIndexBuffer(d2dIndexBuffer, DXGI_FORMAT_R32_UINT, 0);
	//Set the d2d vertex buffer
	d3d11DevCon->IASetVertexBuffers(0, 1, &d2dVertBuffer, &stride, &offset);

	WVP = XMMatrixIdentity();
	cbPerObj.World = XMMatrixTranspose(WVP);
	cbPerObj.WVP = XMMatrixTranspose(WVP);
	d3d11DevCon->UpdateSubresource(cbPerObjectBuffer, 0, NULL, &cbPerObj, 0, 0);
	d3d11DevCon->VSSetConstantBuffers(0, 1, &cbPerObjectBuffer);
	d3d11DevCon->PSSetShaderResources(0, 1, &d2dTexture);
	d3d11DevCon->PSSetSamplers(0, 1, &CubesTexSamplerState);

	d3d11DevCon->RSSetState(m_Blending->CWcullMode);
	//Draw the text box
	d3d11DevCon->DrawIndexed(6, 0, 0);
}

//---------------------------------------------------------DrawSkybox------------------------------------------------------------------------------------------------------------------

void GraphicsClass::DrawSky() {

	//Set the spheres index buffer
	d3d11DevCon->IASetIndexBuffer(m_Sky->sphereIndexBuffer, DXGI_FORMAT_R32_UINT, 0);
	//Set the spheres vertex buffer
	d3d11DevCon->IASetVertexBuffers(0, 1, &m_Sky->sphereVertBuffer, &stride, &offset);

	WVP = m_Sky->sphereWorld * m_Camera->camView * m_Camera->camProjection;
	cbPerObj.WVP = XMMatrixTranspose(WVP);
	cbPerObj.World = XMMatrixTranspose(m_Sky->sphereWorld);
	d3d11DevCon->UpdateSubresource(cbPerObjectBuffer, 0, NULL, &cbPerObj, 0, 0);
	d3d11DevCon->VSSetConstantBuffers(0, 1, &cbPerObjectBuffer);
	//Send our skymap resource view to pixel shader
	d3d11DevCon->PSSetShaderResources(0, 1, &m_Sky->smrv);
	d3d11DevCon->PSSetSamplers(0, 1, &CubesTexSamplerState);
	//Set the new VS and PS shaders
	d3d11DevCon->VSSetShader(m_Sky->SKYMAP_VS, 0, 0);
	d3d11DevCon->PSSetShader(m_Sky->SKYMAP_PS, 0, 0);
	//Set the new depth/stencil and RS states
	d3d11DevCon->OMSetDepthStencilState(m_Sky->DSLessEqual, 0);
	d3d11DevCon->RSSetState(m_Sky->RSCullNone);
	d3d11DevCon->DrawIndexed(m_Sky->NumSphereFaces * 3, 0, 0);
	//Set the default VS shader and depth/stencil state
	d3d11DevCon->VSSetShader(VS, 0, 0);
	d3d11DevCon->OMSetDepthStencilState(NULL, 0);

}

//---------------------------------------------------------DrawLoadedModel------------------------------------------------------------------------------------------------------------------
void GraphicsClass::DrawLoadedModel(LoadModelClass* m_LoadModel) {

	//Draw Loaded Model --------------------------------------------------------------------
	for (int i = 0; i < m_LoadModel->meshSubsets; ++i)
	{
		//Set the grounds index buffer
		d3d11DevCon->IASetIndexBuffer(m_LoadModel->meshIndexBuff, DXGI_FORMAT_R32_UINT, 0);
		//Set the grounds vertex buffer
		d3d11DevCon->IASetVertexBuffers(0, 1, &m_LoadModel->meshVertBuff, &stride, &offset);

		//Set the WVP matrix and send it to the constant buffer in effect file
		WVP					= m_LoadModel->meshWorld * m_Camera->camView * m_Camera->camProjection;
		cbPerObj.WVP		= XMMatrixTranspose(WVP);
		cbPerObj.World		= XMMatrixTranspose(m_LoadModel->meshWorld);
		cbPerObj.difColor	= m_LoadModel->material[m_LoadModel->meshSubsetTexture[i]].difColor;
		cbPerObj.hasTexture = m_LoadModel->material[m_LoadModel->meshSubsetTexture[i]].hasTexture;
		cbPerObj.hasNormMap = m_LoadModel->material[m_LoadModel->meshSubsetTexture[i]].hasNormMap;

		//---
		d3d11DevCon->UpdateSubresource(cbPerObjectBuffer, 0, NULL, &cbPerObj, 0, 0);
		d3d11DevCon->VSSetConstantBuffers(0, 1, &cbPerObjectBuffer);
		d3d11DevCon->PSSetConstantBuffers(1, 1, &cbPerObjectBuffer);

		if (m_LoadModel->material[m_LoadModel->meshSubsetTexture[i]].hasTexture)
			d3d11DevCon->PSSetShaderResources(0, 1, &m_LoadModel->meshSRV[m_LoadModel->material[m_LoadModel->meshSubsetTexture[i]].texArrayIndex]);
		d3d11DevCon->PSSetSamplers(0, 1, &CubesTexSamplerState);

		if (m_LoadModel->material[m_LoadModel->meshSubsetTexture[i]].hasNormMap)
			d3d11DevCon->PSSetShaderResources(1, 1, &m_LoadModel->meshSRV[m_LoadModel->material[m_LoadModel->meshSubsetTexture[i]].normMapTexArrayIndex]);

		d3d11DevCon->RSSetState(m_Sky->RSCullNone);
		int indexStart = m_LoadModel->meshSubsetIndexStart[i];
		int indexDrawAmount = m_LoadModel->meshSubsetIndexStart[i + 1] - m_LoadModel->meshSubsetIndexStart[i];
		if (!m_LoadModel->material[m_LoadModel->meshSubsetTexture[i]].transparent)
			d3d11DevCon->DrawIndexed(indexDrawAmount, indexStart, 0);
	}
}

//---------------------------------------------------------UpdateScene------------------------------------------------------------------------------------------------------------------
void GraphicsClass::UpdateScene(int fpsCounter, float m_delta_time, Packet packet, int P_ID, int UI_index) {


	fps = fpsCounter;
	Player_ID = P_ID;
	delta_time = m_delta_time;
	PacketFromServer = packet;

	//Update Skybox with cam pos
	m_Sky->UpdateSky(m_Camera->camPosition);
	m_UI->StartUI(UI_index);

}

//---------------------------------------------------------DrawScene------------------------------------------------------------------------------------------------------------------

void GraphicsClass::DrawScene(PlayerClass* Player) {

	//Clear our backbuffer
	float bgColor[4] = { (0.0f, 0.0f, 0.0f, 0.0f) };
	d3d11DevCon->ClearRenderTargetView(renderTargetView, bgColor);

	//Refresh the Depth/Stencil view
	d3d11DevCon->ClearDepthStencilView(depthStencilView, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);

	//Draw Light
	m_Light->drawLight(d3d11DevCon);

	//Set our Render Target
	d3d11DevCon->OMSetRenderTargets(1, &renderTargetView, depthStencilView);

	//Set the default blend state (no blending) for opaque objects
	d3d11DevCon->OMSetBlendState(0, 0, 0xffffffff);

	//Reset Vertex and Pixel Shaders
	d3d11DevCon->VSSetShader(VS, 0, 0);
	d3d11DevCon->PSSetShader(PS, 0, 0);

	//Render ground obj
	DrawLoadedModel(m_LoadGround);


	//Render all Player models if every players is ready
	if (PacketFromServer.AllPlayersAreReady) {

		for (int i = 0; i < PacketFromServer.NumOfPlayers; i++) {
			//Player is Alive, Load player and shield model
				if (PacketFromServer.PlayerData[i].isAlive) {
					//Render player model
					DrawLoadedModel(Player->PlayerProp[i].Model_Player);
					if (PacketFromServer.PlayerData[i].isShieldActive && PacketFromServer.PlayerData[i].AmountOfShield > 0)
						//Render player's armor
						DrawLoadedModel(Player->PlayerProp[i].ShieldModel);
				}
				//Render Shooting models
				DrawLoadedModel(Player->PlayerProp[i].ShootObject);

				//Render Ultimate models
				DrawLoadedModel(Player->PlayerProp[i].Ultimate);
		}
		//Renders status UI
		if (Player_ID != 0) 
			RenderStatusUI(Player_ID - 1);

		//Render crosshair for all players
		RenderText(L"", d2dTexture_crosshair, fps, PacketFromServer.PlayerData[0]);

	}

	//Render waiting screen if all players are not ready
	else {
		RenderText(L"", d2dTexture_Waiting, fps , PacketFromServer.PlayerData[0]);
	}

	//Render Skybox
	DrawSky();

	//Present the backbuffer to the screen
	SwapChain->Present(0, 0);


}

void GraphicsClass::RenderStatusUI(int Player_Data_ID) {

	//Display Information UI if player is ALIVE
	if (PacketFromServer.PlayerData[Player_Data_ID].isAlive)
		RenderText(L"FPS: ", d2dTexture, fps, PacketFromServer.PlayerData[Player_Data_ID]);
	else
		//Blood splash when player is dead
		RenderText(L"", d2dTexture_blood, fps, PacketFromServer.PlayerData[Player_Data_ID]);

	//Get damage
	if (PacketFromServer.ID_OF_HIT_PLAYER == Player_Data_ID + 1)
		RenderText(L"", d2dTexture_Damage, fps, PacketFromServer.PlayerData[Player_Data_ID]);

	//Winner
	if (PacketFromServer.ID_OF_WINNER == Player_ID)
		RenderText(L"", d2dTexture_Winner, fps, PacketFromServer.PlayerData[Player_Data_ID]);


}

//---------------------------------------------------------DrawUI------------------------------------------------------------------------------------------------------------------
void GraphicsClass::DrawUI(XMMATRIX cubeWorld, ID3D11ShaderResourceView* CubesTexture, ID3D11SamplerState* CubesTexSamplerState) {

	WVP				= cubeWorld * m_Camera->camView * m_Camera->camProjection;
	cbPerObj.World	= XMMatrixTranspose(cubeWorld);
	cbPerObj.WVP	= XMMatrixTranspose(WVP);
	cbPerObj.hasTexture = true;
	cbPerObj.hasNormMap = false;
	d3d11DevCon->UpdateSubresource(cbPerObjectBuffer, 0, NULL, &cbPerObj, 0, 0);
	d3d11DevCon->VSSetConstantBuffers(0, 1, &cbPerObjectBuffer);
	d3d11DevCon->PSSetShaderResources(0, 1, &CubesTexture);
	d3d11DevCon->PSSetSamplers(0, 1, &CubesTexSamplerState);
	d3d11DevCon->RSSetState(m_Blending->CWcullMode);
	//Draw a model
	d3d11DevCon->DrawIndexed(6, 0, 0);

}

void GraphicsClass::RenderGUI() {

	//Clear our backbuffer
	float bgColor[4] = { (0.0f, 0.0f, 0.0f, 0.0f) };
	d3d11DevCon->ClearRenderTargetView(renderTargetView, bgColor);


	//Refresh the Depth/Stencil view
	d3d11DevCon->ClearDepthStencilView(depthStencilView, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);

	//Draw Light
	m_Light->drawLight(d3d11DevCon);

	//Set our Render Target
	d3d11DevCon->OMSetRenderTargets(1, &renderTargetView, depthStencilView);

	//Set the default blend state (no blending) for opaque objects
	d3d11DevCon->OMSetBlendState(0, 0, 0xffffffff);

	//Reset Vertex and Pixel Shaders
	d3d11DevCon->VSSetShader(VS, 0, 0);
	d3d11DevCon->PSSetShader(PS, 0, 0);

	//Set the cubes index buffer
	d3d11DevCon->IASetIndexBuffer(m_UI->quadIndexBuffer, DXGI_FORMAT_R32_UINT, 0);
	d3d11DevCon->IASetVertexBuffers(0, 1, &m_UI->quadVertBuffer, &stride, &offset);

	//Reset player position to UI
	m_Camera->camPosition	= XMVectorSet(0.0f, 20.0f, -45.0f, 0.0f);
	m_Camera->camTarget		= XMVectorSet(0.0f, 20.0f, 0.0f, 0.0f);
	m_Camera->camUp			= XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f);
	m_Camera->camView		= XMMatrixLookAtLH(m_Camera->camPosition, m_Camera->camTarget, m_Camera->camUp);

	//Render UI canvas
	DrawUI(m_UI->UI_Canvas[0], titleTexture, CubesTexSamplerState);
	DrawUI(m_UI->UI_Canvas[1], playTexture, CubesTexSamplerState);
	DrawUI(m_UI->UI_Canvas[2], quitTexture, CubesTexSamplerState);
	DrawUI(m_UI->UI_Canvas[3], SelectionTexture, CubesTexSamplerState);

	//Render ground obj
	DrawLoadedModel(m_LoadGround);

	//Render Text
	RenderText(L"FPS: ", d2dTexture, fps, PacketFromServer.PlayerData[0]);

	//Render Skybox
	DrawSky();

	SwapChain->Present(0, 0);
}

//---------------------------------------------------------CleanUp------------------------------------------------------------------------------------------------------------------

void GraphicsClass::CleanUp() {

	//Release InitD3D11
	SAFE_RELEASE(SwapChain);
	SAFE_RELEASE(d3d11Device);
	SAFE_RELEASE(d3d11DevCon);
	SAFE_RELEASE(renderTargetView);
	SAFE_RELEASE(cbPerObjectBuffer);

	//Release shaders
	SAFE_RELEASE(VS);
	SAFE_RELEASE(PS);
	SAFE_RELEASE(VS_Buffer);
	SAFE_RELEASE(PS_Buffer);

	//Release Depth
	SAFE_RELEASE(depthStencilView);
	SAFE_RELEASE(depthStencilBuffer);

	//Release d2D - d3D
	SAFE_RELEASE(d3d101Device);
	SAFE_RELEASE(keyedMutex11);
	SAFE_RELEASE(keyedMutex10);
	SAFE_RELEASE(D2DRenderTarget);
	SAFE_RELEASE(Brush);
	SAFE_RELEASE(BackBuffer11);
	SAFE_RELEASE(sharedTex11);
	SAFE_RELEASE(DWriteFactory);
	SAFE_RELEASE(TextFormat);
	SAFE_RELEASE(TextFormatHeader);

	//Release 2D Textures
	SAFE_RELEASE(d2dTexture);
	SAFE_RELEASE(d2dTexture_crosshair);
	SAFE_RELEASE(d2dTexture_blood);
	SAFE_RELEASE(d2dTexture_Damage);
	SAFE_RELEASE(d2dTexture_Waiting);
	SAFE_RELEASE(d2dTexture_Winner);

	//Release UI Textures
	SAFE_RELEASE(titleTexture);
	SAFE_RELEASE(playTexture);
	SAFE_RELEASE(quitTexture);
	SAFE_RELEASE(SelectionTexture);

	//Release 3D Textures
	SAFE_RELEASE(CubesTexSamplerState);



	//Clean obj classes
	m_Blending->CleanUp();
	m_Light->CleanUp();
	m_Sky->CleanUp();
	m_LoadGround->CleanUp();
	m_UI->CleanUp();

	//Delete new obj
	SAFE_DELETE(m_Camera);
	SAFE_DELETE(m_Blending);
	SAFE_DELETE(m_Light);
	SAFE_DELETE(m_Sky);
	SAFE_DELETE(m_LoadGround);
	SAFE_DELETE(m_UI);

}
