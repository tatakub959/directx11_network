#pragma once
#include "d3d11.h"

class ModelClass {
public:
	ModelClass(ID3D11Device* d3d11Device, ID3D11DeviceContext* d3d11DevCon);
	void initModel(ID3D11Device* d3d11Device, ID3D11DeviceContext* d3d11DevCon);
	void UpdateModel(float rotx, float rotz);
	void CleanUp();

public:

	//Square
	ID3D11Buffer* squareIndexBuffer;
	ID3D11Buffer* squareVertBuffer;

	//Cube
	XMMATRIX cubeWorld;

	XMMATRIX RotationX;
	XMMATRIX RotationY;
	XMMATRIX RotationZ;
	XMMATRIX Scale;
	XMMATRIX Translation;
	float rot = 0.01f;


	//Get from Input
	float rotx;
	float rotz;
	float scaleX;
	float scaleY;

	XMMATRIX Rotation;
	XMMATRIX Rotationx;
	XMMATRIX Rotationz;


	struct Vertex
	{
		Vertex() {}
		Vertex(float x, float y, float z, float u, float v, float nx, float ny, float nz, float tx, float ty, float tz) :
			pos(x, y, z), texCoord(u, v), normal(nx, ny, nz), tangent(tx, ty, tz) {}

		XMFLOAT3 pos;
		XMFLOAT2 texCoord;
		XMFLOAT3 normal;

		XMFLOAT3 tangent;
		XMFLOAT3 biTangent;
	};

};