#pragma once
#include "d3d11.h"
class LoadModelClass {

public:
	//mesh variables--------------------------------------------------------------------------
	XMMATRIX meshWorld;
	ID3D11Buffer* meshVertBuff;
	ID3D11Buffer* meshIndexBuff;

	std::vector<XMFLOAT3> meshVertPosArray;
	std::vector<DWORD> meshVertIndexArray;

	int meshSubsets = 0;
	std::vector<int> meshSubsetIndexStart;
	std::vector<int> meshSubsetTexture;

	//Mesh and tex name
	std::vector<ID3D11ShaderResourceView*> meshSRV;
	std::vector<std::wstring> textureNameArray;

	//Bounding variables-----------------------------------------------------------------------
	//Box collision
	std::vector<XMFLOAT3>BoundingBoxVertPosArray;
	std::vector<DWORD> BoundingBoxVertIndexArray;
	XMVECTOR BoundingBoxMinVertex;
	XMVECTOR BoundingBoxMaxVertex;

	//Sphere collision
	float BoundingSphere = 0.0f;
	XMVECTOR CenterOffset;

	XMMATRIX Rotation;
	XMMATRIX Rotation2;
	XMMATRIX camRotationMatrix;
	XMMATRIX Scale;
	XMMATRIX Translation;

	bool isRHCoordSys = true;
	bool computeNormals = true;

	//Shooting Object
	XMVECTOR ShootDir;
	bool objIsShoot = false;
	bool UltimateIsShoot = false;
	float ShootingLifeTime;
	float UltimateLifeTime;

	//Shield
	bool isShiledDisappear = false;

	struct SurfaceMaterial {
		std::wstring matName;
		XMFLOAT4 difColor;
		int texArrayIndex;

		int normMapTexArrayIndex;
		bool hasNormMap;

		bool hasTexture;
		bool transparent;
	};
	std::vector<SurfaceMaterial> material;

	struct Vertex
	{
		Vertex() {}
		Vertex(float x, float y, float z, float u, float v, float nx, float ny, float nz, float tx, float ty, float tz) :
			pos(x, y, z), texCoord(u, v), normal(nx, ny, nz){}

		XMFLOAT3 pos;
		XMFLOAT2 texCoord;
		XMFLOAT3 normal;

		XMFLOAT3 tangent;
		XMFLOAT3 biTangent;
	};

public:
	bool LoadObjModel(std::wstring filename, IDXGISwapChain* SwapChain, ID3D11Device* d3d11Device);

	void initGround();
	void UpdatePlayerWithCameraPosition(float RotCharacter, XMVECTOR camPosition, float camYawRotation);
	void InitialShootObject(XMVECTOR camPosition, XMVECTOR camTarget);
	void InitialShootUltimate(XMVECTOR camPosition, XMVECTOR camTarget);
	void Shoot();
	void ShootUltimate();
	void LoadShield(XMVECTOR camPosition);

	//Bounding
	void CreateBoundingVolumes(std::vector<XMFLOAT3> &vertPosArray, std::vector<XMFLOAT3>& boundingBoxVerts, std::vector<DWORD>& boundingBoxIndex, float &boundingSphere, XMVECTOR &objectCenterOffset);

	bool BoundingSphereCollision(float firstObjBoundingSphere,
		XMVECTOR firstObjCenterOffset,
		XMMATRIX& firstObjWorldSpace,
		float secondObjBoundingSphere,
		XMVECTOR secondObjCenterOffset,
		XMMATRIX& secondObjWorldSpace);

	bool BoundingBoxCollision(XMVECTOR& firstObjBoundingBoxMinVertex,
		XMVECTOR& firstObjBoundingBoxMaxVertex,
		XMVECTOR& secondObjBoundingBoxMinVertex,
		XMVECTOR& secondObjBoundingBoxMaxVertex);

	void CalculateAABB(std::vector<XMFLOAT3> boundingBoxVerts,
		XMMATRIX& worldSpace,
		XMVECTOR& boundingBoxMin,
		XMVECTOR& boundingBoxMax);

	void CleanUp();
};