#include "Input.h"

InputClass::InputClass(HINSTANCE hInstance, HWND hwndM) {
	hwnd = hwndM;

	InitDirecInput(hInstance, hwnd);


}

void InputClass::InitDirecInput(HINSTANCE hInstance, HWND hwnd) {

	DirectInput8Create(hInstance, DIRECTINPUT_VERSION, IID_IDirectInput8, (void**)&DirectInput, NULL);


	DirectInput->CreateDevice(GUID_SysKeyboard, &DIKeyboard, NULL);
	DirectInput->CreateDevice(GUID_SysMouse, &DIMouse, NULL);

	DIKeyboard->SetDataFormat(&c_dfDIKeyboard);
	DIKeyboard->SetCooperativeLevel(hwnd, DISCL_FOREGROUND | DISCL_EXCLUSIVE);

	DIMouse->SetDataFormat(&c_dfDIMouse);
	DIMouse->SetCooperativeLevel(hwnd, DISCL_EXCLUSIVE | DISCL_FOREGROUND);

}

void InputClass::DetectInput(float time) {

	delta_time = time;

	DIMOUSESTATE mouseCurrState;

	//BYTE keyboardState[256];

	DIKeyboard->Acquire();
	DIMouse->Acquire();

	DIMouse->GetDeviceState(sizeof(DIMOUSESTATE), &mouseCurrState);
	DIKeyboard->GetDeviceState(sizeof(keyboardState), (LPVOID)keyboardState);

	//Close window - ESC or select quit button in start menu
	if (KEYDOWN(DIK_ESCAPE) || (isStart && UI_index == 2))
		PostMessage(hwnd, WM_DESTROY, 0, 0);

	//HIT "ENTER"----------------------------------------------
	if (KEYDOWN(DIK_RETURN)) 
		isStart = true;

	if (isStart) {

		//Player movement input
		switch (getMovementInput()){
		//W
		case 0:
			MovingUp_Down_Left_Right(0);
			break;
		//A
		case 1:
			MovingUp_Down_Left_Right(1);
			break;
		//S
		case 2:
			MovingUp_Down_Left_Right(2);
			break;
		//D
		case 3:
			MovingUp_Down_Left_Right(3);
			break;

		//Release W A S D
		default:
			moveLeftRight = 0;
			moveBackForward = 0;
			inputIsPressed = false;
			isMoving = false;
			break;
		}

		//HIT "LSHIFT""-------------------------------------------
		//Check if press LSHIFT (Run) speed will be SuperSpeed
		if (KEYDOWN(DIK_LSHIFT)) {
			Velocity = SuperSpeed;
			isRunning = true;
		}
		else {
			Velocity = NormalSpeed;
			isRunning = false;
		}

		//HIT "SPACE BAR"-------------------------------------------
		if (KEYDOWN(DIK_SPACE))
		{
			inputIsPressed = true;
			ActivatedShield = true;
		}
		else 
			ActivatedShield = false;
		

		//HIT "T"-------------------------------------------
		if (KEYDOWN(DIK_T)) {
			inputIsPressed = true;
			isHealing = true;
		}
		else
			isHealing = false;

		////////////////////////////////////////////////MOUSE INPUT//////////////////////////////////////////

		//Rotate Camera with mouse
		if ((mouseCurrState.lX != mouseLastState.lX) || (mouseCurrState.lY != mouseLastState.lY))
		{
			InputIsEntered += camYaw + camPitch;
			inputIsPressed = true;

			//Look Left-Right
			camYaw += mouseLastState.lX * 0.001f;
			if (camYaw > XM_PI)
				camYaw = -XM_PI;
			if (camYaw < -XM_PI)
				camYaw = XM_PI;

			//Look Up-Down
			camPitch += mouseCurrState.lY * 0.001f;
			if (camPitch > 0.85f)
				camPitch = 0.85f;
			if (camPitch < -0.85f)
				camPitch = -0.85f;

			mouseLastState = mouseCurrState;
		}

		//Left click -------------------------------------------
		if (mouseCurrState.rgbButtons[0])
		{
			inputIsPressed = true;
			LeftClick = true;
			TimeForOneClick += time*5.0f;
			if (TimeForOneClick > time*5.0f) {
				LeftClick = false;
			}
		}
		else
		{
			LeftClick = false;
			TimeForOneClick = 0.0f;
		}

		//Right click -------------------------------------------
		if (mouseCurrState.rgbButtons[1])
		{
			inputIsPressed = true;
			RightClick = true;
			RightOneClick += time*5.0f;
			if (RightOneClick > time*5.0f) {
				RightClick = false;
			}
		}
		else
		{
			RightClick = false;
			RightOneClick = 0.0f;
		}
	}

	else {
		//HIT "Arrow Up"-------------------------------------------
		if (KEYDOWN(DIK_UP))
			UI_index = 1;

		//HIT "Arrow Down"-------------------------------------------
		if (KEYDOWN(DIK_DOWN))
			UI_index = 2;
	}
}

void InputClass::InitInputData() {

	InputIsEntered = 0;
	moveLeftRight = 0.0f;
	moveBackForward = 0.0f;
	camYaw = 0.0f;
	camPitch = 0.0f;
	TimeForOneClick = 0.0f;
	RightOneClick = 0.0f;
	LeftClick = false;
	RightClick = false;
	isStart = false;

	inputIsPressed = false;
	isRunning = false;
	isMoving = false;

	ActivatedShield = false;
}

void InputClass::CleanUp() {

	DIKeyboard->Unacquire();
	DIMouse->Unacquire();
	SAFE_RELEASE(DIKeyboard);
	SAFE_RELEASE(DIMouse);
	SAFE_RELEASE(DirectInput);

}

int InputClass::getMovementInput() {

	if (KEYDOWN(DIK_W)) return 0;
	if (KEYDOWN(DIK_A)) return 1;
	if (KEYDOWN(DIK_S)) return 2;
	if (KEYDOWN(DIK_D)) return 3;

	return -1;
}

void InputClass::MovingUp_Down_Left_Right(int direction) {
	//W or S
	if (direction == 0 || direction == 2) {
		//W
		if(direction == 0)
			moveBackForward = delta_time * Velocity;
		//S
		else
			moveBackForward = -delta_time * Velocity;

		moveLeftRight = 0;
	}
	//A or D
	else {
		//A
		if (direction == 1)
			moveLeftRight = -delta_time* Velocity;
		//D
		else
			moveLeftRight = delta_time* Velocity;

		moveBackForward = 0;
	}

	inputIsPressed = true;
	isMoving = true;
	InputIsEntered += delta_time;
}

