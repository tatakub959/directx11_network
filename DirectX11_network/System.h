#pragma once
#include "d3d11.h"

static bool WndIsActive;

class System {

public:

	System(HINSTANCE hInstance);
	bool Initialize();
	bool InitializeWindow();
	bool messageloop();
	void CleanUp();

public:

	LPCTSTR WndClassName = L"DirectX_Client"; 
	HINSTANCE hInstance;
	HRESULT hr;
	HWND hwnd;
	INT nShowCMD;
	int posX, posY = 0;

};

static LRESULT CALLBACK WndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);