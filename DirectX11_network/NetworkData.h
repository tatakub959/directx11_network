#pragma once
#include <string.h>
#include <xnamath.h>

#define MAX_PACKET_SIZE 1000000

enum PacketTypes {

	INIT_CONNECTION = 0,
	ACTION_EVENT = 1,
	INIT_PLAYER = 2,
	PLAYER_READY = 3,
	GET_INPUT = 4,
	SHOOT = 5,
	PLAYER_HIT = 6,
	HEALING = 7,
	SHOOT_ULTIMATE = 8,
	GAME_END = 9,
};

struct Packet {
	unsigned int packet_type;
	unsigned int player_Id;
	unsigned int NumOfPlayers;
	unsigned int NumAlivePlayers;
	bool AllPlayersAreReady;

	struct Player_Data {

		//Properties
		bool isAlive;
		unsigned int HP;
		float Energy;
		unsigned int AmountOfShield;
		int NumOfHealing;
		unsigned int UltimateGauge;
		bool isHitWithUltimate;
		bool UltiReady;
		

		//Input
		bool ThisPlayerIsReady;
		float InputIsEntered;
		float camYaw;
		bool isLeftClick;
		bool isRightClick;
		bool isShieldActive;
		bool Healing;

		//Camera
		XMVECTOR CameraPosition;
		XMVECTOR camTarget;
	};

	Player_Data PlayerData[4];

	unsigned int ID_OF_SHOOTING;
	unsigned int ID_OF_HIT_PLAYER;
	unsigned int ID_OF_HEALING;
	unsigned int ID_OF_ULTIMATE;
	unsigned int ID_OF_WINNER;

	void serialize(char * data) {
		memcpy(data, this, sizeof(Packet));
	}

	void deserialize(char * data) {
		memcpy(this, data, sizeof(Packet));
	}
};