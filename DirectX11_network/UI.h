#pragma once
#include "d3d11.h"
float const UI_POSITION_Z = -20.0f;
float const BUTT_1_POSITION_Y = 8.5f;
float const BUTT_2_POSITION_Y = 4.5f;

class UIclass {
public:
	//0 = titel game , 1 = start , 2 = quit, 3 = selection outline

	XMMATRIX UI_Canvas[4];
	int UI_Canvas_number = 4;

	ID3D11Buffer *quadVertBuffer;
	ID3D11Buffer *quadIndexBuffer;

	XMMATRIX Scale;
	XMMATRIX Translation;

	struct Vertex
	{
		Vertex() {}
		Vertex(float x, float y, float z, float u, float v, float nx, float ny, float nz, float tx, float ty, float tz) :
			pos(x, y, z), texCoord(u, v), normal(nx, ny, nz) {}

		XMFLOAT3 pos;
		XMFLOAT2 texCoord;
		XMFLOAT3 normal;

		XMFLOAT3 tangent;
		XMFLOAT3 biTangent;
	};

public:

	void initUI(ID3D11Device* d3d11Device, ID3D11DeviceContext* d3d11DevCon, ID3D10Blob* VS_Buffer);
	void StartUI(int UI_index);
	void CleanUp();
};