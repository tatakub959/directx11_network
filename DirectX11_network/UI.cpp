#include "UI.h"
void UIclass::initUI(ID3D11Device* d3d11Device, ID3D11DeviceContext* d3d11DevCon, ID3D10Blob* VS_Buffer) {

	// Create quad
	Vertex v[] =
	{
		// Front Face
		Vertex(-1.0f, -1.0f, -1.0f, 0.0f, 1.0f,-1.0f, -1.0f, -1.0f, 0.0f, 0.0f, 0.0f),
		Vertex(-1.0f,  1.0f, -1.0f, 0.0f, 0.0f,-1.0f,  1.0f, -1.0f, 0.0f, 0.0f, 0.0f),
		Vertex(1.0f,  1.0f, -1.0f, 1.0f, 0.0f, 1.0f,  1.0f, -1.0f, 0.0f, 0.0f, 0.0f),
		Vertex(1.0f, -1.0f, -1.0f, 1.0f, 1.0f, 1.0f, -1.0f, -1.0f, 0.0f, 0.0f, 0.0f),
	};

	DWORD indices[] = {
		// Front Face
		0,  1,  2,
		0,  2,  3,
	};

	D3D11_BUFFER_DESC indexBufferDesc;
	ZeroMemory(&indexBufferDesc, sizeof(indexBufferDesc));

	indexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	indexBufferDesc.ByteWidth = sizeof(DWORD) * 2 * 3;
	indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	indexBufferDesc.CPUAccessFlags = 0;
	indexBufferDesc.MiscFlags = 0;

	D3D11_SUBRESOURCE_DATA iinitData;

	iinitData.pSysMem = indices;
	d3d11Device->CreateBuffer(&indexBufferDesc, &iinitData, &quadIndexBuffer);


	D3D11_BUFFER_DESC vertexBufferDesc;
	ZeroMemory(&vertexBufferDesc, sizeof(vertexBufferDesc));

	vertexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	vertexBufferDesc.ByteWidth = sizeof(Vertex) * 4;
	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vertexBufferDesc.CPUAccessFlags = 0;
	vertexBufferDesc.MiscFlags = 0;

	D3D11_SUBRESOURCE_DATA vertexBufferData;

	ZeroMemory(&vertexBufferData, sizeof(vertexBufferData));
	vertexBufferData.pSysMem = v;
	d3d11Device->CreateBuffer(&vertexBufferDesc, &vertexBufferData, &quadVertBuffer);

}
void UIclass::StartUI(int UI_index) {


	for (int i = 0; i < UI_Canvas_number; i++) {

		Scale = XMMatrixScaling(5.0f, 2.0f, 1.0f);

		UI_Canvas[i] = XMMatrixIdentity();

		switch (i)
		{
		case 0: // Title
			Scale = XMMatrixScaling(20.0f, 8.0f, 1.0f);
			Translation = XMMatrixTranslation(0.0f, 3.5f, UI_POSITION_Z);
			break;
		case 1: // Start button
			Translation = XMMatrixTranslation(0.0f, BUTT_1_POSITION_Y, UI_POSITION_Z);
			break;
		case 2: // Quit button
			Translation = XMMatrixTranslation(0.0f, BUTT_2_POSITION_Y, UI_POSITION_Z);
			break;

		case 3: // Selection overlay
			if (UI_index == 1)
				Translation = XMMatrixTranslation(0.0f, BUTT_1_POSITION_Y, UI_POSITION_Z);
			else
				Translation = XMMatrixTranslation(0.0f, BUTT_2_POSITION_Y, UI_POSITION_Z);
			break;

		}
		UI_Canvas[i] = Translation * Scale;
	}

}


void UIclass::CleanUp() {

	SAFE_RELEASE(quadVertBuffer);
	SAFE_RELEASE(quadIndexBuffer);

}