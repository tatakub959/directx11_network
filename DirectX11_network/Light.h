#pragma once
#include "d3d11.h"

class LightClass {
public:

	void initLight(ID3D11Device* d3d11Device);
	void drawLight(ID3D11DeviceContext* d3d11DevCon);
	void CleanUp();

public:

	ID3D11Buffer* cbPerFrameBuffer;
	ID3D11PixelShader* D2D_PS;
	ID3D10Blob* D2D_PS_Buffer;

	struct Light
	{
		Light()
		{
			ZeroMemory(this, sizeof(Light));
		}

		XMFLOAT3 dir;
		float pad;
		XMFLOAT4 ambient;
		XMFLOAT4 diffuse;
	};

	Light light;

	struct cbPerFrame
	{
		Light  light;
	};

	cbPerFrame constbuffPerFrame;

};