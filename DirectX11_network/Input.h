#pragma once
#include "d3d11.h"

#define KEYDOWN(key) (keyboardState[key] & 0x80)
static const float NormalSpeed = 15.0f;
static const float SuperSpeed = 50.0f;

class InputClass {

public:
	InputClass(HINSTANCE hInstance, HWND hwnd);
	void InitDirecInput(HINSTANCE hInstance, HWND hwnd);
	void DetectInput(float time);
	void InitInputData();
	int getMovementInput();
	void MovingUp_Down_Left_Right(int direction);
	void CleanUp();

public:
	HWND hwnd;

	IDirectInputDevice8* DIKeyboard;
	IDirectInputDevice8* DIMouse;

	DIMOUSESTATE mouseLastState;
	LPDIRECTINPUT8 DirectInput;
	BYTE keyboardState[256];

	//Enter
	bool isStart = false;

	//Detect for pressed input
	bool inputIsPressed = false;


	bool isRunning = false;
	bool isMoving = false;

	//UI
	int UI_index = 1;

	//Active armor
	bool ActivatedShield = false;
	int countSpaceBar;

	//Healing 
	bool isHealing = false;

	//Input get triged
	float InputIsEntered = 0;

	//Mouse click
	bool LeftClick = false;
	bool RightClick = false;
	float TimeForOneClick = 0.0f;
	float RightOneClick = 0.0f;


	//-------------------Character Movement Var -----------------
	XMMATRIX Rotationx;
	XMMATRIX Rotationz;
	
	float delta_time;

	//Moving
	float moveLeftRight = 0.0f;
	float moveBackForward = 0.0f;

	//Rotate mouse
	float camYaw = 0.0f;
	float camPitch = 0.0f;

	float Velocity = 0.0f;

	
};