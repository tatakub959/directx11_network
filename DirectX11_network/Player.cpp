#include "Player.h"

PlayerClass::PlayerClass(HWND hwnd) {
	m_Sounds = new SoundClass();
	m_Sounds->Initialize(hwnd);
}

void PlayerClass::initPlayerModel(IDXGISwapChain* SwapChain, ID3D11Device* d3d11Device) {

	for (int i = 0; i < sizeof(PlayerProp)/sizeof(PlayerProp[0]) ; i++) {
		//Load shoot Object
		PlayerProp[i].ShootObject->LoadObjModel(AssetPath + L"Shoot.obj", SwapChain, d3d11Device);

		//Load shoot Object
		PlayerProp[i].Ultimate->LoadObjModel(AssetPath + L"Ultimate.obj", SwapChain, d3d11Device);

		//Load Shield Object
		PlayerProp[i].ShieldModel->LoadObjModel(AssetPath + L"armor.obj", SwapChain, d3d11Device);

	}

	//Load Player1 Model
	PlayerProp[0].Model_Player->LoadObjModel(AssetPath + L"Wolf.obj", SwapChain, d3d11Device);
		PlayerProp[0].initPosition = XMVectorSet(0.0f, Y_Pos, 0.0f, 0.0f);
			PlayerProp[0].initRotCharacter = XM_PI;
				PlayerProp[0].RotCamera = 0.0f;



	//Load Player2 Model
	PlayerProp[1].Model_Player->LoadObjModel(AssetPath + L"Wolf2.obj", SwapChain, d3d11Device);
		PlayerProp[1].initPosition = XMVectorSet(0.0f, Y_Pos, 100.0f, 0.0f);
			PlayerProp[1].initRotCharacter = 0.0f;
				PlayerProp[1].RotCamera = XM_PI;


	//Load Player3 Model
	PlayerProp[2].Model_Player->LoadObjModel(AssetPath + L"Wolf3.obj", SwapChain, d3d11Device);
		PlayerProp[2].initPosition = XMVectorSet(50.0f, Y_Pos, 50.0f, 0.0f);
			PlayerProp[2].initRotCharacter = XM_PIDIV2;
				PlayerProp[2].RotCamera = -XM_PIDIV2;


	//Load Player4 Model
	PlayerProp[3].Model_Player->LoadObjModel(AssetPath + L"Wolf4.obj", SwapChain, d3d11Device);
		PlayerProp[3].initPosition = XMVectorSet(-50.0f, Y_Pos, 50.0f, 0.0f);
			PlayerProp[3].initRotCharacter = -XM_PIDIV2;
				PlayerProp[3].RotCamera = XM_PIDIV2;

	
}

void PlayerClass::UpdatePlayers(int Player_ID, Packet packet, CameraClass* m_Camera, InputClass* m_Input) {

	PacketFromServer = packet;

	//Update All clients in the same way
	for (int i = 0; i < packet.NumOfPlayers ; i++) {

		if (packet.PlayerData[i].ThisPlayerIsReady && packet.PlayerData[i].isAlive) {

			//Get Player ID
			PlayerProp[i].ID = i + 1;

			//Moving Player camera and update player model's position along with camera position
			if (Player_ID == PlayerProp[i].ID)
				PlayerCamera(packet.PlayerData[i], m_Camera, PlayerProp[i].initPosition, PlayerProp[i].RotCamera, m_Input);
			PlayerProp[i].Model_Player->UpdatePlayerWithCameraPosition(PlayerProp[i].initRotCharacter, packet.PlayerData[i].CameraPosition, packet.PlayerData[i].camYaw);

			//Use abilities of Player
			UseAbilities(PlayerProp[i], packet.PlayerData[i]);

			//Check Collsion
			CollisionDetect(PlayerProp[i]);
		}
	//Sound
		m_Sounds->SoundPlay(m_Input->isMoving , m_Input->isRunning , false , false , false);
	}

	



}
void PlayerClass::PlayerCamera(Packet::Player_Data PlayerData, CameraClass* m_Camera, XMVECTOR initCamPos, float RotCam, InputClass* m_Input) {

	//Set initial camera's position for each player until player begin moving
	if (PlayerData.InputIsEntered == 0) {
		m_Camera->camPosition = initCamPos;
		m_Input->camYaw = 0;
	}
	//If player is alive, can move player's camera
	m_Camera->MovingCamera(m_Input->moveLeftRight, m_Input->moveBackForward, m_Input->camYaw + RotCam, m_Input->camPitch);
	

}

void PlayerClass::UseAbilities(Player_Properties Player, Packet::Player_Data PlayerData) {

	//Shoot normal energy
	if (PlayerData.isLeftClick) {
		//if (PlayerData.Energy == 100 || PlayerData.Energy == 0)
			Player.ShootObject->InitialShootObject(PlayerData.CameraPosition, PlayerData.camTarget);
	}
	Player.ShootObject->Shoot();

	//Shoot ultimate skill
	if (PlayerData.isRightClick){
		//if (PlayerData.UltiReady || PlayerData.UltimateGauge == 100)
			Player.Ultimate->InitialShootUltimate(PlayerData.CameraPosition, PlayerData.camTarget);
		
	}
	Player.Ultimate->ShootUltimate();
	

	//Active armor
	if (PlayerData.isShieldActive) {
		//Update armor position with player position
		Player.ShieldModel->LoadShield(PlayerData.CameraPosition);
		if (PlayerData.AmountOfShield == 0)
			//If armor's HP is zero, can not use armor again
			Player.ShieldModel->isShiledDisappear = true;
		else
			Player.ShieldModel->isShiledDisappear = false;
	}
	

	
}

void PlayerClass::CollisionDetect(Player_Properties &Player) {

	Player.isHit = false;
	Player.isHitUltimate = false;

	for (int i = 0; i < PacketFromServer.NumOfPlayers ; i++) {

		//Not collide with itself
		if (Player.ID != PlayerProp[i].ID) {
			//Get hit on player or armor by NORMAL shooting
			if (Player.Model_Player->BoundingBoxCollision(Player.Model_Player->BoundingBoxMinVertex, Player.Model_Player->BoundingBoxMaxVertex,
														  PlayerProp[i].ShootObject->BoundingBoxMinVertex, PlayerProp[i].ShootObject->BoundingBoxMaxVertex)) {

					Player.isHit = true;
					PlayerProp[i].ShootObject->objIsShoot = false;
					
			}

			//Get hit on player or armor by ULTIMATE shooting
			if (Player.Model_Player->BoundingBoxCollision(Player.Model_Player->BoundingBoxMinVertex, Player.Model_Player->BoundingBoxMaxVertex,
														  PlayerProp[i].Ultimate->BoundingBoxMinVertex, PlayerProp[i].Ultimate->BoundingBoxMaxVertex)) {

					Player.isHit = true;
					Player.isHitUltimate = true;
					PlayerProp[i].Ultimate->UltimateIsShoot = false;
			}

		}

	}
}



void PlayerClass::CleanUp() {

	for (int i = 0; i < sizeof(PlayerProp) / sizeof(PlayerProp[0]); i++) {
		PlayerProp[i].ShootObject->CleanUp();
		PlayerProp[i].Model_Player->CleanUp();
		PlayerProp[i].ShieldModel->CleanUp();
		PlayerProp[i].Ultimate->CleanUp();

		SAFE_DELETE(PlayerProp[i].ShootObject);
		SAFE_DELETE(PlayerProp[i].Model_Player);
		SAFE_DELETE(PlayerProp[i].ShootObject);
		SAFE_DELETE(PlayerProp[i].Model_Player);
	}

	m_Sounds->CleanUp();
	SAFE_DELETE(m_Sounds);
}