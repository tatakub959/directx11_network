#include "System.h"

System::System(HINSTANCE hInstanceM) {

	hInstance = hInstanceM;
	Initialize();

}

bool System::Initialize() {

	InitializeWindow();
	return true;

}

//----------------------------------------------InitializeWindow-------------------------------------------------
bool System::InitializeWindow() {

	//Start creating the window//

	WNDCLASSEX wc;    //Create a new extended windows class
	DEVMODE dmScreenSettings;

	wc.cbSize			= sizeof(WNDCLASSEX);    //Size of our windows class
	wc.style			= CS_HREDRAW | CS_VREDRAW;    //class styles
	wc.lpfnWndProc		= WndProc;    //Default windows procedure function
	wc.cbClsExtra		= NULL;    //Extra bytes after our wc structure
	wc.cbWndExtra		= NULL;    //Extra bytes after our windows instance
	wc.hInstance		= hInstance;    //Instance to current application
	wc.hIcon			= LoadIcon(NULL, IDI_WINLOGO);    //Title bar Icon
	wc.hCursor			= LoadCursor(NULL, IDC_ARROW);    //Default mouse Icon
	wc.hbrBackground	= (HBRUSH)GetStockObject(BLACK_BRUSH);
	wc.lpszMenuName		= NULL;    //Name of the menu attached to our window
	wc.lpszClassName	= WndClassName;    //Name of our windows class
	wc.hIconSm			= LoadIcon(NULL, IDI_WINLOGO); //Icon in your taskbar

	if (!RegisterClassEx(&wc))    //Register windows class
	{
		MessageBox(NULL, L"Error registering class", L"Error", MB_OK | MB_ICONERROR);
		return false;
	}

	// Determine the resolution of the clients desktop screen.
	if (FULL_SCREEN) {

		Width = GetSystemMetrics(SM_CXSCREEN);
		Height = GetSystemMetrics(SM_CYSCREEN);

		// If full screen set the screen to maximum size of the users desktop and 32bit.
		memset(&dmScreenSettings, 0, sizeof(dmScreenSettings));
		dmScreenSettings.dmSize			= sizeof(dmScreenSettings);
		dmScreenSettings.dmPelsWidth	= (unsigned long)Width;
		dmScreenSettings.dmPelsHeight	= (unsigned long)Height;
		dmScreenSettings.dmBitsPerPel	= 32;
		dmScreenSettings.dmFields		= DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT;

		// Change the display settings to full screen.
		ChangeDisplaySettings(&dmScreenSettings, CDS_FULLSCREEN);

		// Set the position of the window to the top left corner.
		posX = posY = 0;
	}
	else
	{
		posX = (GetSystemMetrics(SM_CXSCREEN) - Width) / 2;
		posY = (GetSystemMetrics(SM_CYSCREEN) - Height) / 2;
	}

	//Create Extended Window

	hwnd = CreateWindowEx(
		NULL,    //Extended style
		WndClassName,    //Name of our windows class
		L"DirectX_Client",    //Name in the title bar of our window
		WS_OVERLAPPEDWINDOW,    //style of our window
		posX, posY,    //Top left - Right corner of window
		Width,    //Width of our window
		Height,    //Height of our window
		NULL,    //Handle to parent window
		NULL,    //Handle to a Menu
		hInstance,    //Specifies instance of current program
		NULL    //used for an MDI client window
	);

	if (!hwnd)
	{
		MessageBox(NULL, L"Error creating window", L"Error", MB_OK | MB_ICONERROR);
		return false;
	}

	ShowWindow(hwnd, SW_SHOW);
	UpdateWindow(hwnd);

	return true;
}

////----------------------------------------------------messageloop (Game loop)--------------------------------------------------
bool System::messageloop() {

	MSG msg;
	while (PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
		if (msg.message == WM_QUIT)
		{
			return false;
		}

	}
	return true;
}


////---------------------------------------------Clean------------------------------------------------------------------------
void System::CleanUp() {

	//SAFE_DELETE(m_Timer);

}

////---------------------------------------------WndProc------------------------------------------------------------------------
LRESULT CALLBACK WndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch (msg)    //Check message
	{

	case WM_KEYDOWN:    //For a key down
						//if escape key was pressed, display popup box
		if (wParam == VK_ESCAPE) {
			//if (MessageBox(0, L"Are you sure you want to exit?", L"Exit?", MB_YESNO | MB_ICONQUESTION) == IDYES)
			DestroyWindow(hwnd);
		}

		return 0;

	case WM_DESTROY:    //if x button in top right was pressed
		PostQuitMessage(0);
		return 0;

	case WM_KILLFOCUS:
		WndIsActive = false;
		return 0;

	case WM_SETFOCUS:
		WndIsActive = true;
		return 0;

	}

	//return the message for windows to handle it
	return DefWindowProc(hwnd, msg, wParam, lParam);
}

