#pragma once
#include "LoadOBJ.h"
#include "Camera.h"
#include "Input.h"
#include "Sound.h"

static std::wstring AssetPath = L"..\\Assets\\Models\\";

class PlayerClass {
public:

	Packet PacketFromServer;

	struct Player_Properties {

		unsigned int ID;
		XMVECTOR initPosition;
		float initRotCharacter;
		float RotCamera;
		LoadModelClass* Model_Player	= new LoadModelClass();
		LoadModelClass* ShootObject		= new LoadModelClass();
		LoadModelClass* Ultimate		= new LoadModelClass();
		LoadModelClass* ShieldModel		= new LoadModelClass();
		bool isHit = false;
		bool isHitUltimate = false;
	};
	Player_Properties PlayerProp[4];

	SoundClass* m_Sounds;

	const float Y_Pos = 20.0f;

public:
	PlayerClass(HWND hwnd);

	void initPlayerModel(IDXGISwapChain* SwapChain, ID3D11Device* d3d11Device);

	void UpdatePlayers(int ID, Packet packet, CameraClass* m_Camera, InputClass* m_Input);

	void PlayerCamera(Packet::Player_Data PlayerData, CameraClass* m_Camera, XMVECTOR initCamPos, float RotChar, InputClass* m_Input);

	void UseAbilities(Player_Properties Player, Packet::Player_Data PlayerData);

	void CollisionDetect(Player_Properties &Player);

	void CleanUp();
};