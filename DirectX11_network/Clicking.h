#pragma once
#include "d3d11.h"
#include "UI.h"

class ClickingClass {
public:

	HWND hwnd;

	int ClientWidth = 0;
	int ClientHeight = 0;

	float pickedDist = 0.0f;
	bool isPick = false;

	bool isInUIMode = false;

	UIclass UI;

	float tempDist;

public:

	ClickingClass(HWND hwnd, int ClientWidth, int ClientHeight);

	void pickRayVector(float mouseX, float mouseY, XMVECTOR& pickRayInWorldSpacePos, XMVECTOR& pickRayInWorldSpaceDir, XMMATRIX camProjection, XMMATRIX camView);
	float pick(XMVECTOR pickRayInWorldSpacePos, XMVECTOR pickRayInWorldSpaceDir, std::vector<XMFLOAT3>& vertPosArray, std::vector<DWORD>& indexPosArray, XMMATRIX& worldSpace);
	bool PointInTriangle(XMVECTOR& triV1, XMVECTOR& triV2, XMVECTOR& triV3, XMVECTOR& point);

	void MouseClickUI(bool LeftClick, XMMATRIX camProjection, XMMATRIX camView, int* bottleHit);
};