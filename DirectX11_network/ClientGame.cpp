#include "ClientGame.h"

ClientGame::ClientGame(HINSTANCE hInstance ) {

	m_ClientNetwork		= new ClientNetwork();
	m_WindowApplication = new System(hInstance);
	m_Graphics			= new GraphicsClass(m_WindowApplication->hwnd);
	m_Input				= new InputClass(hInstance, m_WindowApplication->hwnd);

	m_Player			= new PlayerClass(m_WindowApplication->hwnd);
	m_Player->initPlayerModel(m_Graphics->SwapChain, m_Graphics->d3d11Device);

	//Set Limit of delta time
	m_StepTimer.SetFixedTimeStep(true);
	m_StepTimer.SetTargetElapsedSeconds(1.0f / 240.0f);

	sendPackets(PacketTypes::INIT_CONNECTION);
	SelectGameStates(States::START_MENU);
}

void ClientGame::SelectGameStates(States States) {
	GameStates = States;

	if (GameStates == States::START_MENU) {
		Initialization();
	}
	else if (GameStates == States::IN_GAME) {
		GameIsEnd = false;
	}
}

void ClientGame::Initialization() {

	m_Input->InitInputData();
	DeadPlayer_ID.clear();
	AutoRestartTime = 1.0f;

	sendPackets(PacketTypes::INIT_PLAYER);
	

}

bool ClientGame::WindowsIsActive() {
	//Get focus message
	if (WindowActive == m_WindowApplication->hwnd)
		return true;
}	

void ClientGame::RunClient() {

	m_StepTimer.Tick([&]() {

		//Update receiving packet from server
		UpdateClient(m_StepTimer);

		//Render local client's game
		RenderGame(m_StepTimer.GetFramesPerSecond());

		//Player update on local(Update game logic)
		if (!GameIsEnd)
			m_Player->UpdatePlayers(Player_ID, packet, m_Graphics->m_Camera, m_Input);

	});
}

void ClientGame::UpdateClient(DX::StepTimer const& m_StepTimer) {

	delta_time = float(m_StepTimer.GetElapsedSeconds());

	int data_length = m_ClientNetwork->receivePackets(network_data);

	if (data_length > 0) {

		//Get data packet from server
		for (int i = 0; i < (unsigned int)data_length; ) {

			packet.deserialize(&(network_data[i]));
			i += sizeof(Packet);

			switch (packet.packet_type) {

			case PacketTypes::INIT_CONNECTION:
				Player_ID = packet.player_Id;
				break;

			case PacketTypes::INIT_PLAYER:
				break;

			case PacketTypes::PLAYER_READY:
				break;

			case PacketTypes::GET_INPUT:
				break;

			case PacketTypes::SHOOT:
				break;

			case PacketTypes::PLAYER_HIT:
				Client_HIT();
				break;

			case PacketTypes::HEALING:
				break;

			case PacketTypes::SHOOT_ULTIMATE:
				break;

			case PacketTypes::GAME_END:
				break;

			default:
				MessageBox(NULL, L"CLIENT SIDE: Error packet type", L"Error", MB_OK | MB_ICONERROR);
				break;
			}

		}

	}

	//Get focus message
	WindowActive = GetFocus();

	//Get Input in client window, add into packet ,and then send to server in term of packet type
	if (Player_ID != 0) {

		//Control of Player sending packet on client side
		ClientController(packet.PlayerData[Player_ID -1], m_Player->PlayerProp[Player_ID -1], Player_ID);	

		//Set windows position - ID 2: X = 1000 , 3: Z = 450 , 4: X= 1000 Z = 450
		//SetWindowPos(WindowActive, 0, 0, 0, Width, Height, SWP_ASYNCWINDOWPOS);
	}

	
	//If game is end, game will be switch to start menu
	if (GameIsEnd) {
		AutoRestartTime -= delta_time;
		if (AutoRestartTime <= 0) {
			SelectGameStates(States::START_MENU);
			GameIsEnd = false;
			return;
		}
	}

	
}

void ClientGame::sendPackets(PacketTypes packet_type) {

	// send packet
	const unsigned int packet_size = sizeof(Packet);
	char packet_data[packet_size];

	packet.packet_type = packet_type;

	packet.serialize(packet_data);

	NetworkServices::sendMessage(m_ClientNetwork->ConnectSocket, packet_data, packet_size);

}

void ClientGame::RenderGame(int fps) {

	//Update graphics
	m_Graphics->UpdateScene(fps, delta_time, packet, Player_ID, m_Input->UI_index);

	//GUI
	if (GameStates == States::START_MENU) {
		m_Graphics->RenderGUI();
	}
	else {
		//Render all graphics
		m_Graphics->DrawScene(m_Player);
	}

}

void ClientGame::InputClientPacketData(Packet::Player_Data &PlayerData) {

	PlayerData.CameraPosition		= m_Graphics->m_Camera->camPosition;
	PlayerData.camTarget			= m_Graphics->m_Camera->camTarget;
	PlayerData.camYaw				= m_Input->camYaw;
	PlayerData.InputIsEntered		= m_Input->InputIsEntered;
	PlayerData.isLeftClick			= m_Input->LeftClick;
	PlayerData.isRightClick			= m_Input->RightClick;
	PlayerData.isShieldActive		= m_Input->ActivatedShield;
	PlayerData.Healing				= m_Input->isHealing;

}

void ClientGame::ClientController(Packet::Player_Data &PlayerData, PlayerClass::Player_Properties &PlayerProp, int PlayerId) {

	//Checking whether indow is in focus or not
	if (WindowsIsActive())
		m_Input->DetectInput(delta_time);

	//Player press ENTER and select at start button in start menu
	if (m_Input->isStart && m_Input->UI_index == 1) {
		SelectGameStates(States::IN_GAME);

			PlayerData.ThisPlayerIsReady = true;
			//Add input values into each packet's data
			InputClientPacketData(PlayerData);

		if(!packet.AllPlayersAreReady)
			sendPackets(PacketTypes::PLAYER_READY);
	}

	//Sending all event packets
	if (!GameIsEnd) {
		if (packet.AllPlayersAreReady && PlayerData.isAlive == true) {

			//Send ACTION packet to server
			if(m_Input->inputIsPressed) //Only send ACTION packet when player is pressing any input button
				sendPackets(PacketTypes::ACTION_EVENT);

			//Normal Shooting
			if (PlayerData.isLeftClick) {
				packet.ID_OF_SHOOTING = PlayerId;
				sendPackets(PacketTypes::SHOOT);
			}

			//Ultimate Shooting
			if (PlayerData.isRightClick) {
				packet.ID_OF_ULTIMATE = PlayerId;
				sendPackets(PacketTypes::SHOOT_ULTIMATE);
			}

			//Healing
			if (PlayerData.Healing) {
				packet.ID_OF_HEALING = PlayerId;
				sendPackets(PacketTypes::HEALING);
			}

			//Get hit
			if (PlayerProp.isHit) {
				packet.ID_OF_HIT_PLAYER = PlayerId;
					//Hit by ultimate
					PlayerData.isHitWithUltimate = PlayerProp.isHitUltimate;
				sendPackets(PacketTypes::PLAYER_HIT);
			}
		}

		//Game is end when there is only one player left
		if (packet.NumAlivePlayers <= 1) {
			PlayerData.ThisPlayerIsReady = false;
			PlayerData.InputIsEntered = 0;
			sendPackets(PacketTypes::GAME_END);
			GameIsEnd = true;

		}
	}

}

void ClientGame::Client_HIT() {

	for (int i = 0; i < packet.NumOfPlayers; i++) {

		//If get hit and HP is less than 0, that player is dead and reduce number of alive players
		iter = find(DeadPlayer_ID.begin(), DeadPlayer_ID.end(), i);
		//Skip the players that are alreay dead
		if (iter == DeadPlayer_ID.end()) {
			if (packet.PlayerData[i].HP <= 0 || packet.PlayerData[i].HP > 100) {
				//Add id of dead player to list
				DeadPlayer_ID.push_back(i);

				packet.PlayerData[i].HP = 0;
				packet.PlayerData[i].isAlive = false;

				packet.NumAlivePlayers -= 1;

			}
		}
	}

}


void ClientGame::CleanUp() {

	m_WindowApplication->CleanUp();
	m_Graphics->CleanUp();
	m_Player->CleanUp();

	SAFE_DELETE(m_ClientNetwork);
	SAFE_DELETE(m_WindowApplication);
	SAFE_DELETE(m_Graphics);
	SAFE_DELETE(m_Input);
	SAFE_DELETE(m_Player);

}
