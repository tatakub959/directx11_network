#include "Light.h"

void LightClass::initLight(ID3D11Device* d3d11Device) {
	HRESULT hr;

	hr = D3DX11CompileFromFile(L"..\\Assets\\Effects.hlsl", 0, 0, "D2D_PS", "ps_4_0", 0, 0, 0, &D2D_PS_Buffer, 0, 0);
	d3d11Device->CreatePixelShader(D2D_PS_Buffer->GetBufferPointer(), D2D_PS_Buffer->GetBufferSize(), NULL, &D2D_PS);

	//Light information
	light.dir		= XMFLOAT3(0.0f, 5.0f, 0.0f);
	light.ambient	= XMFLOAT4(0.8f, 0.8f, 0.8f, 1.0f);
	light.diffuse	= XMFLOAT4(0.2f, 0.2f, 0.2f, 1.0f);

	//Create the buffer to send to the cbuffer per frame in effect file
	D3D11_BUFFER_DESC cbbd;
	ZeroMemory(&cbbd, sizeof(D3D11_BUFFER_DESC));

	cbbd.Usage		= D3D11_USAGE_DEFAULT;
	cbbd.ByteWidth	= sizeof(cbPerFrame);
	cbbd.BindFlags	= D3D11_BIND_CONSTANT_BUFFER;
	cbbd.CPUAccessFlags = 0;
	cbbd.MiscFlags	= 0;

	d3d11Device->CreateBuffer(&cbbd, NULL, &cbPerFrameBuffer);

}

void LightClass::drawLight(ID3D11DeviceContext* d3d11DevCon) {

	constbuffPerFrame.light = light;
	d3d11DevCon->UpdateSubresource(cbPerFrameBuffer, 0, NULL, &constbuffPerFrame, 0, 0);
	d3d11DevCon->PSSetConstantBuffers(0, 1, &cbPerFrameBuffer);

}

void LightClass::CleanUp() {

	SAFE_RELEASE(cbPerFrameBuffer);
	SAFE_RELEASE(D2D_PS);
	SAFE_RELEASE(D2D_PS_Buffer);

}