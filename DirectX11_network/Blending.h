#pragma once
#include "d3d11.h"

class BlendingClass {
public:
	BlendingClass(ID3D11Device* d3d11Device);
	void initBlending(ID3D11Device* d3d11Device);
	void CleanUp();

public:

	D3D11_RASTERIZER_DESC cmdesc;
	//Blending
	ID3D11BlendState* Transparency;
	ID3D11RasterizerState* CCWcullMode;
	ID3D11RasterizerState* CWcullMode;
};
