#pragma once
#include <WinSock2.h>
#include <Windows.h>
#include "StepTimer.h"
#include "ClientNetwork.h"
#include "NetworkData.h"
#include "System.h"
#include "Graphics.h"
#include "Input.h"
#include "Player.h"
enum States {
	INIT_STATE = 0,
	START_MENU = 1,
	IN_GAME = 2,
};

class ClientGame {

public:

	ClientGame(HINSTANCE hInstance);

	void SelectGameStates(States GameStates);

	void Initialization();

	void RunClient();

	bool WindowsIsActive();

	void sendPackets(PacketTypes packet_type);

	char network_data[MAX_PACKET_SIZE];

	void UpdateClient(DX::StepTimer const& m_StepTimer);

	void RenderGame(int fps);

	void InputClientPacketData(Packet::Player_Data &Player);

	void ClientController(Packet::Player_Data &PlayerData, PlayerClass::Player_Properties &Player , int PlayerId);

	void Client_HIT();

	void CleanUp();

public:

	Packet packet;

	DX::StepTimer m_StepTimer;

	ClientNetwork* m_ClientNetwork;
	System* m_WindowApplication;
	GraphicsClass* m_Graphics;
	InputClass* m_Input;
	PlayerClass* m_Player;

	HWND WindowActive;
	unsigned int Player_ID;
	float delta_time;

	float AutoRestartTime = 3.0f;

	bool GameIsEnd = false;

	std::vector<int> DeadPlayer_ID;
	std::vector<int>::iterator iter;

	int GameStates = States::INIT_STATE;

};
