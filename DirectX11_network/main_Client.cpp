#include "ClientGame.h"

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nShowCmd) {

	//init client
	ClientGame* client = new ClientGame(hInstance);

	while (client->m_WindowApplication->messageloop()) {
		//Run client side
		client->RunClient();
	}

	client->CleanUp();
	SAFE_DELETE(client);

	return 0;

}
